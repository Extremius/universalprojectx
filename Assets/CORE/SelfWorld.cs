﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Core.Imported;
using Core.Terrain;
using System;
using System.Threading;
using System.Threading.Tasks;

using Vector3Int = Core.Vector3Int;

namespace Core
{
    public class SelfWorld : IRunnable
    {
        private const int chunksSize = RuntimeChunk.chunkSize;

        #region fields
        private const int chunkSize = RuntimeChunk.chunkSize;

        //private readonly object _listSync = new object();
        private readonly object _dictionarySync = new object();

        //player position in world
        private Vector3Long _playerChunkPosition;
        private Vector3Int _playerInChunkPosition;

        //chunks loaded
        private uint chunksUpdated = 0;
        private Vector3Int chunksLoadedDistance = new Vector3Int(4, 2, 4); //дистанция для загруженных чанков
        private float renderDistance = 1;//0..1 процент чанков для рендера (чанки могут быть загружены но неактивны)
        private float simulationDistance = 1;//0..1 процент чанков симуляции (за пределами, чанки не обновляются)
        //позволяет экономить ресурсы на симуляции, рендеря например 10 чанков в длинну и изменять лишь 4

        Dictionary<Vector3Long, RuntimeChunk> loadedChunks;
        List<Vector3Long> requestToLoad, requestUnload;
        List<IMeshBuildSupport> nearChunks = new List<IMeshBuildSupport>(27);
        #endregion

        #region props
        public IList<IMeshBuildSupport> NearChunks => nearChunks;
        public IReadOnlyList<Vector3Long> RequestToLoad => requestToLoad;
        public IReadOnlyList<Vector3Long> RequestToUnload => requestUnload;
        public IReadOnlyDictionary<Vector3Long, RuntimeChunk> Chunks => loadedChunks;
        public object _ChunkCollectionSync => _dictionarySync;
        //public object _RequestCollectionSync => _listSync;
        public bool ChunksUpdated(ref uint value)
        {
            if (value != chunksUpdated)
            {
                value = chunksUpdated;
                return true;
            }
            else return false;
        }
        public Vector3Long PlayerChunkPosition
        {
            get => _playerChunkPosition;
            set
            {
                if(value != _playerChunkPosition)
                {
                    _playerChunkPosition = value;
                    chunkPosChanged = true;
                }
            }
        }
        #endregion

        #region constructors
        public SelfWorld(Vector3Long playerChunkPos)
        {
            var size = chunksLoadedDistance.x * chunksLoadedDistance.y * chunksLoadedDistance.z;
            loadedChunks = new Dictionary<Vector3Long, RuntimeChunk>(size);
            requestToLoad = new List<Vector3Long>(size);
            requestUnload = new List<Vector3Long>(size);

            _playerChunkPosition = playerChunkPos;
        }
        #endregion

        #region world options
        private int groundHeight = 50;

        #endregion

        #region behaviour
        private void UpdateNearChunks()
        {
            lock(nearChunks)
            {
                nearChunks.Clear();

                nearChunks.Add(loadedChunks[PlayerChunkPosition]);

                for (int y = -1; y <= 1; y++)
                    for (int z = -1; z <= 1; z++)
                        for (int x = -1; x <= 1; x++)
                        {
                            if (x == 0)
                                if (x == y)
                                    if (x == z)
                                        continue;

                            nearChunks.Add(loadedChunks[PlayerChunkPosition + new Vector3Long(x, y, z)]);
                        }
            }
        }
        private void UpdateMeta()
        {
            lock (_dictionarySync)
            {
                requestToLoad.Clear();
                requestUnload.Clear();

                for (int y = -chunksLoadedDistance.y; y <= chunksLoadedDistance.y; y++)
                    for (int z = -chunksLoadedDistance.z; z <= chunksLoadedDistance.z; z++)
                        for (int x = -chunksLoadedDistance.x; x <= chunksLoadedDistance.x; x++)
                        {
                            Vector3Long chunkPos = _playerChunkPosition + new Vector3Int(x, y, z);
                            requestToLoad.Add(chunkPos);
                        }

                foreach (var pair in loadedChunks)
                {
                    requestUnload.Add(pair.Key);//fill all dirty
                }

                foreach (var pair in loadedChunks)
                {
                    var key = pair.Key;
                    if (requestToLoad.Contains(key))
                    {
                        requestToLoad.Remove(key);
                        requestUnload.Remove(key);
                    }
                }
            }
            //find request loading, unloading and nothing
        }

        private void LoadRequestsChunks()
        {
            if (requestToLoad.Count is 0)
                return;
            
            lock(_dictionarySync)
            {
                var locker = new object();
                GenerateChunksRange(requestToLoad, locker);

                Thread.Sleep(100);
                lock(locker)
                {
                    return;
                }
            }
        }

        private void UnloadChunks()
        {
            if (requestUnload.Count is 0)
                return;

            lock(_dictionarySync)
            {
                var pass = 1f / 3f / RequestToUnload.Count;
                foreach(var pos in requestUnload)
                {
                    if (loadedChunks.TryGetValue(pos, out var chunk))
                    {
                        loadedChunks.Remove(pos);
                        chunk.Dispose();
                    }
                    progress += pass;
                }

                GC.Collect();
            }
        }

        //Main generator
        private RuntimeChunk GenerateChunk(Vector3Long selfPosition)
        {
            RuntimeBlock[,,] blocks = new RuntimeBlock[chunkSize, chunkSize, chunkSize];
            var mdl = new LightModel(100, new Vector2USH(0,0), 0.8f);
            var chunk = RuntimeChunk.LazyCreatedChunk(selfPosition);
            RuntimeChunk.SetBlocksDataIntoLazyChunk(chunk, blocks);
            for (byte y = 0; y < chunkSize; y++)
                for (byte z = 0; z < chunkSize; z++)
                    for (byte x = 0; x < chunkSize; x++)
                    {
                        var height = y + selfPosition.y * chunkSize;
                        mdl.SetGlobalIntensity(Mathf.Clamp((height * 100f / groundHeight) * 0.4f, 30, 100));
                        
                        {
                            if (height > groundHeight)
                            {
                                blocks[x, y, z] = new RuntimeBlock(new Vector3Byte(x, y, z), 0, 0, mdl);//air
                            }
                            else if (height > groundHeight - 6)
                            {
                                blocks[x, y, z] = new RuntimeBlock(new Vector3Byte(x, y, z), 0, 1, mdl);//grass
                            }
                            else
                            {
                                blocks[x, y, z] = new RuntimeBlock(new Vector3Byte(x, y, z), 0, 2, mdl);//stone
                            }
                        }

                        
                    }
            if (selfPosition == new Vector3Long(0, 1, 0))
            {
                byte z = 2, x = 2;
                for (byte y = 18; y < 18+5; y++)
                {
                    x = 2;
                    for (z = 2; z < 31; z++)
                    {
                        blocks[x, y, z] = new RuntimeBlock(new Vector3Byte(x, y, z), 0, 2, mdl);//stone
                    }
                    x = 30;
                    for (z = 2; z < 31; z++)
                    {
                        blocks[x, y, z] = new RuntimeBlock(new Vector3Byte(x, y, z), 0, 2, mdl);//stone
                    }

                    z = 2;
                    for (x = 2; x < 31; x++)
                    {
                        blocks[x, y, z] = new RuntimeBlock(new Vector3Byte(x, y, z), 0, 2, mdl);//stone
                    }
                    z = 30;
                    for (x = 2; x < 31; x++)
                    {
                        blocks[x, y, z] = new RuntimeBlock(new Vector3Byte(x, y, z), 0, 2, mdl);//stone
                    }
                }

                for (z = 2; z < 30; z++)
                for (x = 2; x < 30; x++)
                        blocks[x, 18 + 5, z] = new RuntimeBlock(new Vector3Byte(x, 18+5, z), 0, 2, mdl);//stone
            }
            return chunk;
        }

        private void GenerateChunksRange(IList<Vector3Long> positions, object locker)
        {
            var pass = 1f / 3f / positions.Count;
            lock(locker)
            {
                var _dictWriteAccessSync = new object();

                Task pass0 = Task.Run(Pass_0);
                Task pass1 = Task.Run(Pass_1);

                Task.WaitAll(pass0, pass1);

                return;

                #region internal methods
                void Pass_0()
                {
                    //четное
                    for(int i =0;i<positions.Count;i+=2)
                    {
                        var newChunk = GenerateChunk(positions[i]);
                        WriteNewChunk(newChunk);
                    }
                }
                void Pass_1()
                {
                    //не четное
                    for (int i = 1; i < positions.Count; i += 2)
                    {
                        var newChunk = GenerateChunk(positions[i]);
                        WriteNewChunk(newChunk);
                    }
                }

                void WriteNewChunk(RuntimeChunk chunk)
                {
                    lock (_dictWriteAccessSync)
                    {
                        loadedChunks.Add(chunk.ChunkPosition, chunk);
                        progress += pass;
                    }
                }
                #endregion
            }
        }


        #endregion

        #region access
        public RuntimeBlock? NeighborBlock(Vector3Long globalIndex, Rotate side)
        {
            Vector3Long modSide = new Vector3Long(0, 0, 0);

            switch (side)
            {
                case Rotate.North:
                    modSide.z = 1;
                    break;
                case Rotate.East:
                    modSide.x = 1;
                    break;
                case Rotate.South:
                    modSide.z = -1;
                    break;
                case Rotate.West:
                    modSide.x = -1;
                    break;
                case Rotate.Up:
                    modSide.y = 1;
                    break;
                case Rotate.Down:
                    modSide.y = -1;
                    break;
            }

            return ReadBlock(globalIndex + modSide);
        }
        public void FillBlock(Vector3Long chunk, RuntimeBlock? data)
        {
            if (data.HasValue)
            {
                //lock (_dictionarySync)
                
                    if (loadedChunks.TryGetValue(chunk, out var runtimeCH))
                    {
                        runtimeCH.FillBlock(data);
                    }
                
            }
        }
        public void FillBlockUnsafe(Vector3Long chunk, RuntimeBlock data)
        {
            if (loadedChunks.TryGetValue(chunk, out var runtimeCH))
            {
                runtimeCH.FillBlock(data);
            }
        }
        public void FillBlockUnsafeDirectly(Vector3Long blockGlobalPos, RuntimeBlock data)
        {
            if (loadedChunks.TryGetValue(WorldGenerator.GetChunkPos(blockGlobalPos), out var runtimeCH))
            {
                runtimeCH.FillBlock(data);
            }
        }
        public void FillBlocks(Vector3Long chunk, params RuntimeBlock[] data)
        {
            if (data != null || data.Length > 0)
            {
                //lock (_dictionarySync)
                
                    if (loadedChunks.TryGetValue(chunk, out var runtimeCH))
                    {
                        runtimeCH.FillBlocks(data);
                    }
                
            }
        }
        public RuntimeBlock? ReadBlock(Vector3Long globalIndex)
        {
            Vector3Long chunk = globalIndex / chunkSize;

            Vector3Int pos;

            pos.x = Math.Abs((int)(globalIndex.x % chunkSize));
            pos.y = Math.Abs((int)(globalIndex.y % chunkSize));
            pos.z = Math.Abs((int)(globalIndex.z % chunkSize));

            const int reverseSize = chunkSize - 1;
            if (globalIndex.x < 0)
            {
                chunk.x -= 1;
                pos.x = reverseSize - pos.x;
            }
            if (globalIndex.y < 0)
            {
                chunk.y -= 1;
                pos.y = reverseSize - pos.y;
            }
            if (globalIndex.z < 0)
            {
                chunk.z -= 1;
                pos.z = reverseSize - pos.z;
            }
            //if (globalIndex.x >= 0) //x
            //    pos.x = (int)(globalIndex.x % RuntimeChunk.chunkSize);
            //else
            //    pos.x = (RuntimeChunk.chunkSize + (int)(globalIndex.x % RuntimeChunk.chunkSize));

            //if (globalIndex.y >= 0) //y
            //    pos.y = (int)(globalIndex.y % RuntimeChunk.chunkSize);
            //else
            //    pos.y = (RuntimeChunk.chunkSize + (int)(globalIndex.y % RuntimeChunk.chunkSize));

            //if (globalIndex.z >= 0) //z
            //    pos.z = (int)(globalIndex.z % RuntimeChunk.chunkSize);
            //else
            //    pos.z = (RuntimeChunk.chunkSize + (int)(globalIndex.z % RuntimeChunk.chunkSize));

            ////fix
            //if (pos.x == RuntimeChunk.chunkSize)
            //    pos.x = RuntimeChunk.chunkSize - 1;
            //if (pos.y == RuntimeChunk.chunkSize)
            //    pos.y = RuntimeChunk.chunkSize - 1;
            //if (pos.z == RuntimeChunk.chunkSize)
            //    pos.z = RuntimeChunk.chunkSize - 1;

            if (loadedChunks.TryGetValue(chunk, out var runtimeCH))
            {
                return runtimeCH.Read(pos);
            }
            else return null;
        }
        public RuntimeBlock ReadBlockUnsafe(Vector3Long globalIndex)
        {
            Vector3Long chunk = globalIndex / chunkSize;

            Vector3Int pos;

            pos.x = Math.Abs((int)(globalIndex.x % chunkSize));
            pos.y = Math.Abs((int)(globalIndex.y % chunkSize));
            pos.z = Math.Abs((int)(globalIndex.z % chunkSize));

            const int reverseSize = chunkSize - 1;
            if (globalIndex.x < 0)
            {
                chunk.x -= 1;
                pos.x = reverseSize - pos.x;
            }
            if (globalIndex.y < 0)
            {
                chunk.y -= 1;
                pos.y = reverseSize - pos.y;
            }
            if (globalIndex.z < 0)
            {
                chunk.z -= 1;
                pos.z = reverseSize - pos.z;
            }





            //if (globalIndex.x >= 0) //x
            //    pos.x = (int)(globalIndex.x % RuntimeChunk.chunkSize);
            //else
            //    pos.x = RuntimeChunk.chunkSize + (int)(globalIndex.x % RuntimeChunk.chunkSize);

            //if (globalIndex.y >= 0) //y
            //    pos.y = (int)(globalIndex.y % RuntimeChunk.chunkSize);
            //else
            //    pos.y = RuntimeChunk.chunkSize + (int)(globalIndex.y % RuntimeChunk.chunkSize);

            //if (globalIndex.z >= 0) //z
            //    pos.z = (int)(globalIndex.z % RuntimeChunk.chunkSize);
            //else
            //    pos.z = RuntimeChunk.chunkSize + (int)(globalIndex.z % RuntimeChunk.chunkSize);

            if (loadedChunks.TryGetValue(chunk, out var runtimeCH))
                {
                    return runtimeCH.Read(pos);
                }
                else return RuntimeBlock.Empty;
            
        }
        #endregion

        #region IRunnable support
        public bool IsRunning => isRunning;
        private bool isRunning = false;
        Thread mainThread;
        bool chunkPosChanged = true;
        public void Execute()
        {
            if(!IsRunning)
            {
                isRunning = true;

                mainThread = new Thread(loop);
                mainThread.Name = "World Generator";
                mainThread.Start();
            }
        }

        public void Terminate()
        {
            if (isRunning)
            {
                isRunning = false;
                mainThread.Join(5000);

            }
        }

        System.Diagnostics.Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();
        private float progress = 0;
        public float Progress => progress;
        void loop()
        {
            while(isRunning)
            {
                if (chunkPosChanged)
                {
                    try
                    {
                        progress = 0;
                        watch.Restart();

                        UpdateMeta();
                        progress += 1f / 3f;

                        LoadRequestsChunks();

                        UnloadChunks();

                        UpdateNearChunks();

                        chunkPosChanged = false;
                        chunksUpdated++;

                        Debug.Log($"Chunks update/generate request time: {watch.Elapsed.TotalMilliseconds:f0} ms\nTotal chunks count: {loadedChunks.Count}\nGenerated chunks count: {requestToLoad.Count}\nUnloaded chunks count: {requestUnload.Count}");
                    }
                    catch(Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
                else Thread.Sleep(1000);
            }
        }

        #endregion
        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты).
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                Terminate();
                // TODO: задать большим полям значение NULL.

                disposedValue = true;
            }
        }

        //TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        ~SelfWorld()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(false);
        }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
