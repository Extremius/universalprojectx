﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace Core.Binary
{
    /// <summary>
    /// Класс, представляющий ветвистый тег объекта или группы объектов, поддерживающий форматирования типа string UTF-16 или double числа.
    /// Поддерживает перечисление в формате string
    /// </summary>
    [System.Serializable]
    public sealed class BinaryTreeTag : IEnumerable<string>
    {
        private static readonly char[] separator = new char[]{'/','\\' };
        private readonly object _sync = new object();
        private string name;
        public string Name { get => name; set => name = value; }
        private Dictionary<string, _IBinaryTree> branches;

        #region constructor
        public BinaryTreeTag(string lable)
        {
            name = lable;
            branches = new Dictionary<string, _IBinaryTree>();
        }
        public BinaryTreeTag(string lable, int branchesCount)
        {
            name = lable;
            if (branchesCount < 1) branchesCount = 1;
            branches = new Dictionary<string, _IBinaryTree>(branchesCount);
        }
        #endregion
        #region behaviour
        public void SetValueTo(string direction, double value)
        {
            SetValueTo(direction, value.ToString(System.Globalization.CultureInfo.InvariantCulture));
        }
        public void SetValueTo(string direction, string value) //like main/attributes/health:40
        {
            lock (_sync)
            {
                var split = direction.Split(separator);
                _IBinaryTree inBranch = null;
                TreeTag previoslyBranch = null;

                for (int depth = 0; depth < split.Length; depth++)
                {
                    var branchName = split[depth];
                    //first
                    if (depth is 0)
                    {
                        if (!branches.TryGetValue(branchName, out inBranch))
                        {
                            //no branch
                            if (depth != split.Length - 1)
                            {
                                //no last
                                var newBranch = new TreeTag(branchName, 4);

                                branches.Add(branchName, newBranch);

                                previoslyBranch = newBranch;
                                continue;
                            }
                            else
                            {
                                //is end
                                var newBranch = new ValueTag(branchName, value);

                                branches.Add(branchName, newBranch);

                                inBranch = newBranch;
                                return;
                            }
                        }
                        else
                        {
                            //has branch
                            if (depth != split.Length - 1)
                            {
                                //no end
                                previoslyBranch = inBranch as TreeTag;
                            }
                            else
                            {
                                if (inBranch is ValueTag tag)
                                {
                                    tag.RawValue = value;
                                    return;
                                }
                                else throw new InvalidProgramException("Unspecified exception");
                            }
                        }
                    }
                    else
                    {
                        //get next branch
                        if (previoslyBranch.TryGetBranch(branchName, out inBranch))
                        {
                            //exist
                            if (depth != split.Length - 1)
                            {
                                //no last
                                previoslyBranch = inBranch as TreeTag;
                                continue;
                            }
                            else
                            {
                                //is end
                                if (inBranch is ValueTag tag)
                                {
                                    tag.RawValue = value;
                                    return;
                                }
                                else throw new InvalidProgramException("Unspecified exception");
                            }
                        }
                        else
                        {
                            //no exists
                            if (depth != split.Length - 1)
                            {
                                //no last
                                previoslyBranch = previoslyBranch.AddNewBranch(branchName);
                                continue;
                            }
                            else
                            {
                                //is end
                                previoslyBranch.SetValueString(branchName, value); //modificatble
                                return;
                            }
                        }
                    }
                }
            }
        }
        public double GetDoubleValueFrom(string direction)
        {
            var split = direction.Split(separator);
            _IBinaryTree inBranch = null;
            TreeTag previoslyBranch = null;

            for (int depth = 0; depth < split.Length; depth++)
            {
                var branchName = split[depth];
                //first
                if (depth is 0)
                {
                    if (!branches.TryGetValue(branchName, out inBranch))
                    {
                        //no branch
                        if (depth != split.Length - 1)
                        {
                            lock (_sync)
                            {
                                //no last
                                var newBranch = new TreeTag(branchName, 4);

                                branches.Add(branchName, newBranch);

                                previoslyBranch = newBranch;
                            }
                            continue;
                        }
                        else
                        {
                            //is end
                            lock (_sync)
                            {
                                var newBranch = new ValueTag(branchName, 0);

                                branches.Add(branchName, newBranch);

                                inBranch = newBranch;
                            }
                            return 0;
                        }
                    }
                    else
                    {
                        //has branch
                        if (depth != split.Length - 1)
                        {
                            //no end
                            previoslyBranch = inBranch as TreeTag;
                        }
                        else
                        {
                            if (inBranch is ValueTag tag)
                            {
                                return tag.DoubleValue;
                            }
                            else throw new InvalidProgramException("Unspecified exception");
                        }
                    }
                }
                else
                {
                    //get next branch
                    if (previoslyBranch.TryGetBranch(branchName, out inBranch))
                    {
                        //exist
                        if (depth != split.Length - 1)
                        {
                            //no last
                            previoslyBranch = inBranch as TreeTag;
                            continue;
                        }
                        else
                        {
                            //is end
                            if (inBranch is ValueTag tag)
                            {
                                return tag.DoubleValue;
                            }
                            else throw new InvalidProgramException("Unspecified exception");
                        }
                    }
                    else
                    {
                        lock (_sync)
                        {
                            //no exists
                            if (depth != split.Length - 1)
                            {
                                //no last

                                previoslyBranch = previoslyBranch.AddNewBranch(branchName);

                                continue;
                            }
                            else
                            {
                                //is end

                                previoslyBranch.SetValueDouble(branchName, 0); //modificatble

                                return 0;
                            }
                        }
                    }
                }
            }

            throw new InvalidProgramException("Unspecified exception");
        }

        public string GetRawValueFrom(string direction)
        {
            var split = direction.Split(separator);
            _IBinaryTree inBranch = null;
            TreeTag previoslyBranch = null;

            for (int depth = 0; depth < split.Length; depth++)
            {
                var branchName = split[depth];
                //first
                if (depth is 0)
                {
                    if (!branches.TryGetValue(branchName, out inBranch))
                    {
                        //no branch
                        if (depth != split.Length - 1)
                        {
                            //no last
                            lock (_sync)
                            {
                                var newBranch = new TreeTag(branchName, 4);

                                branches.Add(branchName, newBranch);

                                previoslyBranch = newBranch;
                            }
                            continue;
                        }
                        else
                        {
                            //is end
                            lock (_sync)
                            {
                                var newBranch = new ValueTag(branchName, 0);

                                branches.Add(branchName, newBranch);

                                inBranch = newBranch;
                            }
                            return "";
                        }
                    }
                    else
                    {
                        //has branch
                        if (depth != split.Length - 1)
                        {
                            //no end
                            previoslyBranch = inBranch as TreeTag;
                        }
                        else
                        {
                            if (inBranch is ValueTag tag)
                            {
                                return tag.RawValue;
                            }
                            else throw new InvalidProgramException("Unspecified exception");
                        }
                    }
                }
                else
                {
                    //get next branch
                    if (previoslyBranch.TryGetBranch(branchName, out inBranch))
                    {
                        //exist
                        if (depth != split.Length - 1)
                        {
                            //no last
                            previoslyBranch = inBranch as TreeTag;
                            continue;
                        }
                        else
                        {
                            //is end
                            if (inBranch is ValueTag tag)
                            {
                                return tag.RawValue;
                            }
                            else throw new InvalidProgramException("Unspecified exception");
                        }
                    }
                    else
                    {
                        //no exists
                        lock (_sync)
                        {
                            if (depth != split.Length - 1)
                            {
                                //no last
                                previoslyBranch = previoslyBranch.AddNewBranch(branchName);
                                continue;
                            }
                            else
                            {
                                //is end
                                previoslyBranch.SetValueString(branchName, ""); //modificatble
                                return "";
                            }
                        }
                    }
                }
            }

            throw new InvalidProgramException("Unspecified exception");
        }


        #endregion
        #region IEnumerable support
        IEnumerator<string> IEnumerable<string>.GetEnumerator()
        {
            lock(_sync)
            {
                foreach(var binaryBranch in branches)
                {
                    if(binaryBranch.Value is ValueTag vg)
                    {
                        yield return vg.RawValue;
                    }
                    else if(binaryBranch.Value is TreeTag tg)
                    {
                        foreach(var res in tg)
                        {
                            yield return res;
                        }
                    }
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (this as IEnumerable<string>).GetEnumerator();
        }
        #endregion

        #region internal classes
        private interface _IBinaryTree
        {
            string Name { get; set; }

            double DoubleValue { get; set; }
            string RawValue { get; set; }

            _IBinaryTree GetBranch(string name);
        }
        private class TreeTag : _IBinaryTree, IEnumerable<string>
        {
            private string name;
            private Dictionary<string, _IBinaryTree> tree;

            public string Name { get => name; set => name = value; }
            public double DoubleValue { get => throw new MethodAccessException(); set => throw new MethodAccessException(); }
            public string RawValue { get => throw new MethodAccessException(); set => throw new MethodAccessException(); }
            
            public _IBinaryTree GetBranch(string branchName)
            {
                lock (tree)
                {
                    return tree[branchName];
                }
            }

            public TreeTag AddNewBranch(string newBranchName)
            {
                if (tree.ContainsKey(newBranchName))
                    throw new ArgumentException("Данный тег уже существует в ветке");

                var branch = new TreeTag(newBranchName, 4);
                tree.Add(newBranchName, branch);
                return branch;

            }
            public void SetValueString(string valueName, string value)
            {
                if (tree.TryGetValue(valueName, out var val))
                {
                    (val as ValueTag).RawValue = value;
                }
                else
                {
                    var branch = new ValueTag(valueName, value);
                    tree.Add(valueName, branch);
                }
            }
            public void SetValueDouble(string valueName, double value)
            {
                if (tree.TryGetValue(valueName, out var val))
                {
                    (val as ValueTag).DoubleValue = value;
                }
                else
                {
                    var branch = new ValueTag(valueName, value);
                    tree.Add(valueName, branch);
                }
            }
            public ValueTag GetValue(string valueName)
            {
                if (tree.TryGetValue(valueName, out var val))
                {
                    return (val as ValueTag);
                }
                else
                {
                    var branch = new ValueTag(valueName, 0);
                    tree.Add(valueName, branch);

                    return branch;
                }
            }
            public bool RemoveBranch(string branchName)
            {
                lock(tree)
                {
                    return tree.Remove(branchName);
                }
            }
            public bool TryGetBranch(string branchName, out _IBinaryTree branch)
            {
                return tree.TryGetValue(branchName, out branch);
            }

            IEnumerator<string> IEnumerable<string>.GetEnumerator()
            {
                foreach (var pair in tree)
                {
                    if (pair.Value is ValueTag vg)
                    {
                        yield return vg.RawValue;
                    }
                    else if(pair.Value is TreeTag tg)
                    {
                        foreach(var res in tg)
                        {
                            yield return res; 
                        }
                    }
                }
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return (this as IEnumerable<string>).GetEnumerator();
            }

            public TreeTag(string branchName, int arrayCount)
            {
                name = branchName;
                tree = new Dictionary<string, _IBinaryTree>(arrayCount);
            }
        }
        private class ValueTag : _IBinaryTree
        {
            private string name;
            private string rawValue;

            private double? cashValue;

            public string Name { get => name; set => name = value; }
            public string RawValue
            {
                get => rawValue;
                set
                {
                    if (rawValue.Equals(value))
                        return;

                    if (double.TryParse(value, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out var doubleVal))
                    {
                        cashValue = doubleVal;
                    }
                    else cashValue = null;

                    rawValue = value;
                }
            }

            public double DoubleValue
            {
                get
                {
                    if (!cashValue.HasValue)
                        throw new MethodAccessException("Значение не представляется как число");
                    return cashValue.Value;
                }
                set
                {
                    cashValue = value;
                    rawValue = value.ToString(System.Globalization.CultureInfo.InvariantCulture);
                }
            }

            public _IBinaryTree GetBranch(string @null) => throw new MethodAccessException();

            public ValueTag(string name, string value)
            {
                this.name = name;
                rawValue = "";
                cashValue = null;

                RawValue = value;
            }
            public ValueTag(string name, double value)
            {
                this.name = name;
                rawValue = "";
                cashValue = null;

                DoubleValue = value;
            }
        }
        #endregion
    }
}
