﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Core.Terrain
{
    //old size = 21+120 = 141 bytes
    //new size = 8+26 = 34 bytes

    public struct RuntimeBlock
    {
        public static readonly RuntimeBlock Empty = new RuntimeBlock(new Vector3Byte(0, 0, 0), 0, 0, new LightModel(0, new Vector2USH(0, 0), 1));

        //Position and rotation
        public Vector3Byte localPosition;
        public Rotate rotation;

        //block statistic
        //public int bttReference;
        //public uint modelType; //render specialModel? zero is simple
        private LightModel lightModel;
        public uint typeReference;

        public Vector3Long Position(Vector3Long chunkPosition)
        {
            return chunkPosition * RuntimeChunk.chunkSize + localPosition;
            
        }
        public LightModel Light
        {
            get => lightModel;
            set => lightModel = value;
        }

        public RuntimeBlock(Vector3Byte localPosition, Rotate rotation ,uint typeReference, LightModel model)
        {
            this.localPosition = localPosition;
            this.typeReference = typeReference;
            lightModel = model;
            this.rotation = rotation;
        }

        public void CopyFrom(RuntimeBlock source)
        {
            this.localPosition = source.localPosition;
            this.rotation = source.rotation;
            lightModel = source.lightModel;
            typeReference = source.typeReference;
        }
    }

    [Flags]
    public enum LightSettings : byte
    {
        None =0x0,
        Transparent =0x1,
        Void =0x2,
    }

    //old size = 66 bytes
    //new size = 26 bytes
    public struct LightModel 
    {
        public USingleHalf reflectionLight; //note positive value change lightIntensity on render, negative value create side is transparent, zero consume all light
        public Side North, East, South, West, Up, Down;
        public LightSettings settings;
        public void SetGlobalIntensity(float globalIntensity)
        {
            North.globalIntensity =
            East.globalIntensity =
            South.globalIntensity =
            West.globalIntensity =
            Up.globalIntensity =
            Down.globalIntensity = (USingleHalf)(globalIntensity * 0.01f);
        }
        public void SetGlobalIntensity(USingleHalf globalIntensity)
        {
            North.globalIntensity =
            East.globalIntensity =
            South.globalIntensity =
            West.globalIntensity =
            Up.globalIntensity =
            Down.globalIntensity = globalIntensity;
        }

        public void SetLocalIntensity(float localIntensity, ushort lightID)
        {
            var intensity = (USingleHalf)(localIntensity * 0.01f);

            North.SetLocalIntensity(intensity, lightID);
            East.SetLocalIntensity(intensity, lightID);
            South.SetLocalIntensity(intensity, lightID);
            West.SetLocalIntensity(intensity, lightID);
            Up.SetLocalIntensity(intensity, lightID);
            Down.SetLocalIntensity(intensity, lightID);
        }
        public void SetLocalIntensity(USingleHalf localIntensity, ushort lightID)
        {
            North.SetLocalIntensity(localIntensity, lightID);
            East.SetLocalIntensity(localIntensity, lightID);
            South.SetLocalIntensity(localIntensity, lightID);
            West.SetLocalIntensity(localIntensity, lightID);
            Up.SetLocalIntensity(localIntensity, lightID);
            Down.SetLocalIntensity(localIntensity, lightID);
        }

        public struct Side //4 bytes
        {
            //public float localIntensity, globalIntensity;
            public USingleHalf globalIntensity;
            internal USingleHalf _localIntensity;
            public USingleHalf intensityOnSide => globalIntensity > _localIntensity ? globalIntensity : _localIntensity;
            //public Vector2USH directionFromSide;
            internal ushort lightID;

            public Side(USingleHalf localIntensity, USingleHalf globalIntensity, Vector2USH directionFromSide)
            {
                this._localIntensity = localIntensity;
                this.globalIntensity = globalIntensity;
                this.lightID = 0;
                //this.directionFromSide = directionFromSide;
                //this.reflectionLight = reflectionLight;
            }

            public USingleHalf localIntensity => _localIntensity;
            public void SetLocalIntensity(USingleHalf intensity, ushort lightID)
            {
                if(_localIntensity < intensity)
                {
                    _localIntensity = intensity;
                    this.lightID = lightID;
                }
            }
            public void ClearLocalIntensity(ushort lightID)
            {
                if (this.lightID == lightID)
                    _localIntensity = (USingleHalf)USingleHalf.MinValue;
            }
        }

        public LightModel(float globalIntesnity, Vector2USH polarDirection, float reflection)
        {
            settings = 0;
            reflectionLight = (USingleHalf)reflection;
            North = East = South = West = Up = Down = new Side((USingleHalf)0f, (USingleHalf)(globalIntesnity*0.01f), polarDirection);
        }

        public Vector3 GetLightNormal(Vector3 localPoint)
        {
            //x
            if (localPoint.x > 0.49f)
                return Vector3.right;
            if (localPoint.x < -0.49f)
                return Vector3.left;

            //y
            if (localPoint.y > 0.49f)
                return Vector3.up;
            if (localPoint.y < -0.49f)
                return Vector3.down;

            //z
            if (localPoint.z > 0.49f)
                return Vector3.forward;
            if (localPoint.z < -0.49f)
                return Vector3.back;

            return Vector3.zero;
        }

        public Side GetSideFromNormal(Vector3 normal)
        {
            if (normal == Vector3.right)
                return East;
            if (normal == Vector3.left)
                return West;
            if (normal == Vector3.up)
                return Up;
            if (normal == Vector3.down)
                return Down;
            if (normal == Vector3.forward)
                return North;
            if (normal == Vector3.back)
                return South;

            //Debug.Log($"Side nit found! invalid normal! {normal.ToString()}");
            return new Side();
        }

        public void SetSideFromNormal(Vector3 normal, Side side)
        {
            if (normal == Vector3.right)
                East = side;
            if (normal == Vector3.left)
                West = side;
            if (normal == Vector3.up)
                Up = side;
            if (normal == Vector3.down)
                Down = side;
            if (normal == Vector3.forward)
                North = side;
            if (normal == Vector3.back)
                South = side;
        }
    }
}
