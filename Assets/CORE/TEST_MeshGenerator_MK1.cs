﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Core.Binary;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Experimental
{
    public enum BlockRotation
    {
        Up,
        Left,
        Down,
        Right,
    }
    public class TEST_MeshGenerator_MK1 : MonoBehaviour
    {
        BinaryTreeTag tree = new BinaryTreeTag("test");
        [SerializeField] MeshFilter filter;

        Vector3[] verties = new Vector3[]
            {
                new Vector3(0,0),
                new Vector3(0,1),
                new Vector3(1,1),
                new Vector3(1,0),
            };
        int[] triangles = new int[]
        {
            0,3,1,3,2,1
        };

        [SerializeField] Vector2[] uv0;
        [SerializeField] Vector4[] uv2;
       

        const float obs = 1f / 6f;
        [SerializeField, Header("Options")] Vector2Int blockID;
        [SerializeField] Vector2Int lightID;
        [SerializeField] int lightStrength = 256;
        [SerializeField] BlockRotation rotation = BlockRotation.Left;
        [SerializeField] bool useAutoGenerateUV = true;
        public void GenerateMesh()
        {
            var mesh = new Mesh();

            mesh.vertices = verties;
            RecalculateUVsForBlockID(uv0, blockID);
            mesh.uv = uv0;
            RecalculateLightUVsForBlockID(uv2, lightID, lightStrength);
            mesh.SetUVs(1, uv2);
            mesh.triangles = triangles;

            filter.sharedMesh = mesh;


        }

        private void RecalculateUVsForBlockID(Vector2[] uv, Vector2Int blockID)
        {
            if (!useAutoGenerateUV)
                return;

            switch (rotation)
            {
                case BlockRotation.Right:
                    uv[0] = new Vector2(blockID.x * obs, blockID.y * obs);
                    uv[1] = new Vector2(blockID.x * obs, (blockID.y + 1) * obs);
                    uv[2] = new Vector2((blockID.x + 1) * obs, (blockID.y + 1) * obs);
                    uv[3] = new Vector2((blockID.x + 1) * obs, blockID.y * obs);
                    break;
                case BlockRotation.Down:
                    uv[1] = new Vector2(blockID.x * obs, blockID.y * obs);
                    uv[2] = new Vector2(blockID.x * obs, (blockID.y + 1) * obs);
                    uv[3] = new Vector2((blockID.x + 1) * obs, (blockID.y + 1) * obs);
                    uv[0] = new Vector2((blockID.x + 1) * obs, blockID.y * obs);
                    break;
                case BlockRotation.Left:
                    uv[2] = new Vector2(blockID.x * obs, blockID.y * obs);
                    uv[3] = new Vector2(blockID.x * obs, (blockID.y + 1) * obs);
                    uv[0] = new Vector2((blockID.x + 1) * obs, (blockID.y + 1) * obs);
                    uv[1] = new Vector2((blockID.x + 1) * obs, blockID.y * obs); break;
                case BlockRotation.Up:
                    uv[3] = new Vector2(blockID.x * obs, blockID.y * obs);
                    uv[0] = new Vector2(blockID.x * obs, (blockID.y + 1) * obs);
                    uv[1] = new Vector2((blockID.x + 1) * obs, (blockID.y + 1) * obs);
                    uv[2] = new Vector2((blockID.x + 1) * obs, blockID.y * obs);
                    break;
            }
            
        }

        const float derivate = 1f / 256f;
        private void RecalculateLightUVsForBlockID(Vector4[] uv, Vector2Int blockID, int lightStrength)
        {
            if (!useAutoGenerateUV)
                return;
            var floatStrength = 1f-(lightStrength / 256f);
            uv[0] = new Vector4(blockID.x * obs, blockID.y * obs, floatStrength, 0);
            uv[1] = new Vector4(blockID.x * obs, (blockID.y + 1) * obs, floatStrength, 1);
            uv[2] = new Vector4((blockID.x + 1) * obs, (blockID.y + 1) * obs, floatStrength+derivate, 1);
            uv[3] = new Vector4((blockID.x + 1) * obs, blockID.y * obs, floatStrength+derivate, 0);
        }
    }

    public struct CyclicInt
    {
        private int val, min,max;

        public int ToMax => max - val;
        public int ToMin => val - min;

        public int Value
        {
            get => val;
            set
            {
                if (value < min)
                    val = min;
                else if (value > max)
                    val = max;
                else val = value;
            }
        }

        public int Min => min;
        public int Max => max;

        public int Change(int delta)
        {
            var ret = val;
            var sum = val + delta;
            if (sum > max)
            {
                var del = sum - max;
                val = min + del;
            }
            else if (sum < min)
            {
                var del = min - sum;
                val = max - del;
            }
            else
                val = sum;
            return ret;
        }

        public CyclicInt (int value, int maxValue, int minValue =0)
        {
            max = maxValue;
            min = minValue;
            val = min;
            Value = value;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(TEST_MeshGenerator_MK1))]
    public class TEST_MeshGenerator_MK1_Editor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if(GUILayout.Button("Reload Mesh"))
            {
                (target as TEST_MeshGenerator_MK1).GenerateMesh();
            }
        }
    }
#endif
}
