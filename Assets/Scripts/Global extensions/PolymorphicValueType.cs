﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Extensions
{
    /// <summary>
    /// Структура, позволяющая хранить большинство неуправляемых типов. Так же поддерживает операции приведения и изменения
    /// </summary>
    [Serializable]
    public readonly struct DynamicValue : IConvertible
    {
        private readonly TypeCode type;
        private readonly decimal datasheet;

        #region fabric
        private DynamicValue(TypeCode type, decimal value)
        {
            this.type = type;
            datasheet = value;
        }
        /// <summary>
        /// Создает новый экземпляр с конкретным значением и типом. Небезопасно.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DynamicValue Force(TypeCode type, decimal value)
        {
            switch(type)
            {
                default:
                    return new DynamicValue(type, value);
                case TypeCode.Char:
                case TypeCode.DateTime:
                case TypeCode.Empty:
                case TypeCode.DBNull:
                case TypeCode.Object:
                    throw new InvalidCastException($"Неподдерживаемый тип {type}");
            }
        }
        public DynamicValue(decimal value)
        {
            this.datasheet = value;
            type = TypeCode.Decimal;
        }
        public DynamicValue(double value)
        {
            this.datasheet = (decimal)value;
            type = TypeCode.Double;
        }
        public DynamicValue(float value)
        {
            this.datasheet = (decimal)value;
            type = TypeCode.Single;
        }
        public DynamicValue(long value)
        {
            this.datasheet = (decimal)value;
            type = TypeCode.Int64;
        }
        public DynamicValue(int value)
        {
            this.datasheet = (decimal)value;
            type = TypeCode.Int32;
        }
        public DynamicValue(short value)
        {
            this.datasheet = (decimal)value;
            type = TypeCode.Int16;
        }
        public DynamicValue(byte value)
        {
            this.datasheet = (decimal)value;
            type = TypeCode.Byte;
        }
        public DynamicValue(sbyte value)
        {
            this.datasheet = (decimal)value;
            type = TypeCode.SByte;
        }
        public DynamicValue(bool value)
        {
            this.datasheet = value ? 1 : 0;
            type = TypeCode.Boolean;
        }
        public DynamicValue(ushort value)
        {
            this.datasheet = (decimal)value;
            type = TypeCode.UInt16;
        }
        public DynamicValue(uint value)
        {
            this.datasheet = (decimal)value;
            type = TypeCode.UInt32;
        }
        public DynamicValue(ulong value)
        {
            this.datasheet = (decimal)value;
            type = TypeCode.UInt64;
        }
        #endregion
        private static InvalidCastException NewInvalidCastException() => new InvalidCastException($"Неверное преобразование типов");
        #region explicit casts //явное
        public static explicit operator byte (DynamicValue value)
        {
            if (value.type == TypeCode.Byte)
            {
                return (byte)value.datasheet;
            }
            else throw NewInvalidCastException();
        }
        public static explicit operator sbyte(DynamicValue value)
        {
            if (value.type == TypeCode.SByte)
            {
                return (sbyte)value.datasheet;
            }
            else throw NewInvalidCastException();
        }
        public static explicit operator bool(DynamicValue value)
        {
            if (value.type == TypeCode.Boolean)
            {
                return decimal.Round(value.datasheet) is 0 ? false : true;
            }
            else throw NewInvalidCastException();
        }
        public static explicit operator short(DynamicValue value)
        {
            if (value.type == TypeCode.Int16)
            {
                return (short)value.datasheet;
            }
            else throw NewInvalidCastException();
        }
        public static explicit operator ushort(DynamicValue value)
        {
            if (value.type == TypeCode.UInt16)
            {
                return (ushort)value.datasheet;
            }
            else throw NewInvalidCastException();
        }
        public static explicit operator int(DynamicValue value)
        {
            if (value.type == TypeCode.Int32)
            {
                return (int)value.datasheet;
            }
            else throw NewInvalidCastException();
        }
        public static explicit operator uint(DynamicValue value)
        {
            if (value.type == TypeCode.UInt32)
            {
                return (uint)value.datasheet;
            }
            else throw NewInvalidCastException();
        }
        public static explicit operator long(DynamicValue value)
        {
            if (value.type == TypeCode.Int64)
            {
                return (long)value.datasheet;
            }
            else throw NewInvalidCastException();
        }
        public static explicit operator ulong(DynamicValue value)
        {
            if (value.type == TypeCode.UInt64)
            {
                return (ulong)value.datasheet;
            }
            else throw NewInvalidCastException();
        }
        public static explicit operator float(DynamicValue value)
        {
            if (value.type == TypeCode.Single)
            {
                return (float)value.datasheet;
            }
            else throw NewInvalidCastException();
        }
        public static explicit operator double(DynamicValue value)
        {
            if (value.type == TypeCode.Double)
            {
                return (double)value.datasheet;
            }
            else throw NewInvalidCastException();
        }
        public static explicit operator decimal(DynamicValue value)
        {
            if (value.type == TypeCode.Decimal)
            {
                return (decimal)value.datasheet;
            }
            else throw NewInvalidCastException();
        }
        #endregion

        #region implicit casts //неявное
        public static implicit operator DynamicValue (byte value) => new DynamicValue(value);
        public static implicit operator DynamicValue (sbyte value) => new DynamicValue(value);
        public static implicit operator DynamicValue (bool value) => new DynamicValue(value);
        public static implicit operator DynamicValue (short value) => new DynamicValue(value);
        public static implicit operator DynamicValue (ushort value) => new DynamicValue(value);
        public static implicit operator DynamicValue (int value) => new DynamicValue(value);
        public static implicit operator DynamicValue (uint value) => new DynamicValue(value);
        public static implicit operator DynamicValue (long value) => new DynamicValue(value);
        public static implicit operator DynamicValue (ulong value) => new DynamicValue(value);
        public static implicit operator DynamicValue (float value) => new DynamicValue(value);
        public static implicit operator DynamicValue (double value) => new DynamicValue(value);
        public static implicit operator DynamicValue (decimal value) => new DynamicValue(value);
        #endregion

        #region IConvert interface
        public TypeCode GetTypeCode() => type;
        public bool ToBoolean(IFormatProvider provider) => (bool)this;
        public byte ToByte(IFormatProvider provider) => (byte)this;
        public char ToChar(IFormatProvider provider) => throw NewInvalidCastException();
        public DateTime ToDateTime(IFormatProvider provider) => throw NewInvalidCastException();
        public decimal ToDecimal(IFormatProvider provider) => (decimal)this;
        public double ToDouble(IFormatProvider provider) => (double)this;
        public short ToInt16(IFormatProvider provider) => (short)this;
        public int ToInt32(IFormatProvider provider) => (int)this;
        public long ToInt64(IFormatProvider provider) => (long)this;
        public sbyte ToSByte(IFormatProvider provider) => (sbyte)this;
        public float ToSingle(IFormatProvider provider) => (float)this;
        public string ToString(IFormatProvider provider) => this.ToString();
        public object ToType(Type conversionType, IFormatProvider provider) => throw NewInvalidCastException();
        public ushort ToUInt16(IFormatProvider provider) => (ushort)this;
        public uint ToUInt32(IFormatProvider provider) => (uint)this;
        public ulong ToUInt64(IFormatProvider provider) => (ulong)this;
        #endregion

        #region overrides
        public override string ToString()
        {
            switch (type)
            {
                case TypeCode.Boolean:
                    return ((bool)this).ToString();
                case TypeCode.Byte:
                    return ((byte)this).ToString();
                case TypeCode.SByte:
                    return ((sbyte)this).ToString();
                case TypeCode.Int16:
                    return ((short)this).ToString();
                case TypeCode.UInt16:
                    return ((ushort)this).ToString();
                case TypeCode.Int32:
                    return ((int)this).ToString();
                case TypeCode.UInt32:
                    return ((uint)this).ToString();
                case TypeCode.Int64:
                    return ((long)this).ToString();
                case TypeCode.UInt64:
                    return ((ulong)this).ToString();
                case TypeCode.Single:
                    return ((float)this).ToString();
                case TypeCode.Double:
                    return ((double)this).ToString();
                case TypeCode.Decimal:
                    return ((decimal)this).ToString();

                default: return base.ToString();
            }

        }

        public override bool Equals(object obj)
        {
            if(obj is DynamicValue val)
            {
                if (this.type == val.type)
                    if (this.datasheet == val.datasheet)
                        return true;
            }
            return false;
        }
        public bool Equals(DynamicValue obj)
        {
            if (this.type == obj.type)
                if (this.datasheet == obj.datasheet)
                    return true;
            return false;
        }
        public override int GetHashCode()
        {
            return datasheet.GetHashCode();
        }
        #endregion

        #region operators
        //equals
        public static bool operator ==(DynamicValue left, DynamicValue right)
        {
            return left.Equals(right);
        }
        public static bool operator !=(DynamicValue left, DynamicValue right)
        {
            return !left.Equals(right);
        }
        //logic
        public static bool operator >(DynamicValue left, DynamicValue right)
        {
            return left.datasheet> right;
        }
        public static bool operator >=(DynamicValue left, DynamicValue right)
        {
            return left.datasheet >= right;
        }
        public static bool operator <(DynamicValue left, DynamicValue right)
        {
            return left.datasheet < right;
        }
        public static bool operator <=(DynamicValue left, DynamicValue right)
        {
            return left.datasheet <= right;
        }

        public static DynamicValue operator +(DynamicValue left, DynamicValue right)
        {
            if (left.type == right.type)
            {
                switch (left.type)
                {
                    default:
                        throw NewInvalidCastException();
                    case TypeCode.Byte:
                        checked{
                            decimal result = (left.datasheet + right.datasheet);
                            return new DynamicValue((byte)result);
                        }
                    case TypeCode.SByte:
                        checked
                        {
                            decimal result = (left.datasheet + right.datasheet);
                            return new DynamicValue((sbyte)result);
                        }
                    case TypeCode.Int16:
                        checked
                        {
                            decimal result = (left.datasheet + right.datasheet);
                            return new DynamicValue((short)result);
                        }
                    case TypeCode.Int32:
                        checked
                        {
                            decimal result = (left.datasheet + right.datasheet);
                            return new DynamicValue((int)result);
                        }
                    case TypeCode.Int64:
                        checked
                        {
                            decimal result = (left.datasheet + right.datasheet);
                            return new DynamicValue((long)result);
                        }
                    case TypeCode.UInt16:
                        checked
                        {
                            decimal result = (left.datasheet + right.datasheet);
                            return new DynamicValue((ushort)result);
                        }
                    case TypeCode.UInt32:
                        checked
                        {
                            decimal result = (left.datasheet + right.datasheet);
                            return new DynamicValue((uint)result);
                        }
                    case TypeCode.UInt64:
                        checked
                        {
                            decimal result = (left.datasheet + right.datasheet);
                            return new DynamicValue((ulong)result);
                        }
                    case TypeCode.Single:
                        checked
                        {
                            decimal result = (left.datasheet + right.datasheet);
                            return new DynamicValue((float)result);
                        }
                    case TypeCode.Double:
                        checked
                        {
                            decimal result = (left.datasheet + right.datasheet);
                            return new DynamicValue((double)result);
                        }
                    case TypeCode.Decimal:
                        checked
                        {
                            decimal result = (left.datasheet + right.datasheet);
                            return new DynamicValue(result);
                        }
                }
            }
            else throw NewInvalidCastException();
        }
        public static DynamicValue operator -(DynamicValue left, DynamicValue right)
        {
            if (left.type == right.type)
            {
                switch (left.type)
                {
                    default:
                        throw NewInvalidCastException();

                    case TypeCode.Byte:
                        checked
                        {
                            decimal result = (left.datasheet - right.datasheet);
                            return new DynamicValue((byte)result);
                        }
                    case TypeCode.SByte:
                        checked
                        {
                            decimal result = (left.datasheet - right.datasheet);
                            return new DynamicValue((sbyte)result);
                        }
                    case TypeCode.Int16:
                        checked
                        {
                            decimal result = (left.datasheet - right.datasheet);
                            return new DynamicValue((short)result);
                        }
                    case TypeCode.Int32:
                        checked
                        {
                            decimal result = (left.datasheet - right.datasheet);
                            return new DynamicValue((int)result);
                        }
                    case TypeCode.Int64:
                        checked
                        {
                            decimal result = (left.datasheet - right.datasheet);
                            return new DynamicValue((long)result);
                        }
                    case TypeCode.UInt16:
                        checked
                        {
                            decimal result = (left.datasheet - right.datasheet);
                            return new DynamicValue((ushort)result);
                        }
                    case TypeCode.UInt32:
                        checked
                        {
                            decimal result = (left.datasheet - right.datasheet);
                            return new DynamicValue((uint)result);
                        }
                    case TypeCode.UInt64:
                        checked
                        {
                            decimal result = (left.datasheet - right.datasheet);
                            return new DynamicValue((ulong)result);
                        }
                    case TypeCode.Single:
                        checked
                        {
                            decimal result = (left.datasheet - right.datasheet);
                            return new DynamicValue((float)result);
                        }
                    case TypeCode.Double:
                        checked
                        {
                            decimal result = (left.datasheet - right.datasheet);
                            return new DynamicValue((double)result);
                        }
                    case TypeCode.Decimal:
                        checked
                        {
                            decimal result = (left.datasheet - right.datasheet);
                            return new DynamicValue(result);
                        }
                }
            }
            else throw NewInvalidCastException();
        }
        public static DynamicValue operator *(DynamicValue left, DynamicValue right)
        {
            if (left.type == right.type)
            {
                switch (left.type)
                {
                    default:
                        throw NewInvalidCastException();

                    case TypeCode.Byte:
                        checked
                        {
                            decimal result = (left.datasheet * right.datasheet);
                            return new DynamicValue((byte)result);
                        }
                    case TypeCode.SByte:
                        checked
                        {
                            decimal result = (left.datasheet * right.datasheet);
                            return new DynamicValue((sbyte)result);
                        }
                    case TypeCode.Int16:
                        checked
                        {
                            decimal result = (left.datasheet * right.datasheet);
                            return new DynamicValue((short)result);
                        }
                    case TypeCode.Int32:
                        checked
                        {
                            decimal result = (left.datasheet * right.datasheet);
                            return new DynamicValue((int)result);
                        }
                    case TypeCode.Int64:
                        checked
                        {
                            decimal result = (left.datasheet * right.datasheet);
                            return new DynamicValue((long)result);
                        }
                    case TypeCode.UInt16:
                        checked
                        {
                            decimal result = (left.datasheet * right.datasheet);
                            return new DynamicValue((ushort)result);
                        }
                    case TypeCode.UInt32:
                        checked
                        {
                            decimal result = (left.datasheet * right.datasheet);
                            return new DynamicValue((uint)result);
                        }
                    case TypeCode.UInt64:
                        checked
                        {
                            decimal result = (left.datasheet * right.datasheet);
                            return new DynamicValue((ulong)result);
                        }
                    case TypeCode.Single:
                        checked
                        {
                            decimal result = (left.datasheet * right.datasheet);
                            return new DynamicValue((float)result);
                        }
                    case TypeCode.Double:
                        checked
                        {
                            decimal result = (left.datasheet * right.datasheet);
                            return new DynamicValue((double)result);
                        }
                    case TypeCode.Decimal:
                        checked
                        {
                            decimal result = (left.datasheet * right.datasheet);
                            return new DynamicValue(result);
                        }
                }
            }
            else throw NewInvalidCastException();
        }
        public static DynamicValue operator /(DynamicValue left, DynamicValue right)
        {
            if (left.type == right.type)
            {
                switch (left.type)
                {
                    default:
                        throw NewInvalidCastException();

                    case TypeCode.Byte:
                        checked
                        {
                            decimal result = (left.datasheet / right.datasheet);
                            return new DynamicValue((byte)result);
                        }
                    case TypeCode.SByte:
                        checked
                        {
                            decimal result = (left.datasheet / right.datasheet);
                            return new DynamicValue((sbyte)result);
                        }
                    case TypeCode.Int16:
                        checked
                        {
                            decimal result = (left.datasheet / right.datasheet);
                            return new DynamicValue((short)result);
                        }
                    case TypeCode.Int32:
                        checked
                        {
                            decimal result = (left.datasheet / right.datasheet);
                            return new DynamicValue((int)result);
                        }
                    case TypeCode.Int64:
                        checked
                        {
                            decimal result = (left.datasheet / right.datasheet);
                            return new DynamicValue((long)result);
                        }
                    case TypeCode.UInt16:
                        checked
                        {
                            decimal result = (left.datasheet / right.datasheet);
                            return new DynamicValue((ushort)result);
                        }
                    case TypeCode.UInt32:
                        checked
                        {
                            decimal result = (left.datasheet / right.datasheet);
                            return new DynamicValue((uint)result);
                        }
                    case TypeCode.UInt64:
                        checked
                        {
                            decimal result = (left.datasheet / right.datasheet);
                            return new DynamicValue((ulong)result);
                        }
                    case TypeCode.Single:
                        checked
                        {
                            decimal result = (left.datasheet / right.datasheet);
                            return new DynamicValue((float)result);
                        }
                    case TypeCode.Double:
                        checked
                        {
                            decimal result = (left.datasheet / right.datasheet);
                            return new DynamicValue((double)result);
                        }
                    case TypeCode.Decimal:
                        checked
                        {
                            decimal result = (left.datasheet /
 right.datasheet);
                            return new DynamicValue(result);
                        }
                }
            }
            else throw NewInvalidCastException();
        }
        public static DynamicValue operator %(DynamicValue left, DynamicValue right)
        {
            if (left.type == right.type)
            {
                switch (left.type)
                {
                    default:
                        throw NewInvalidCastException();

                    case TypeCode.Byte:
                        checked
                        {
                            decimal result = (left.datasheet % right.datasheet);
                            return new DynamicValue((byte)result);
                        }
                    case TypeCode.SByte:
                        checked
                        {
                            decimal result = (left.datasheet % right.datasheet);
                            return new DynamicValue((sbyte)result);
                        }
                    case TypeCode.Int16:
                        checked
                        {
                            decimal result = (left.datasheet % right.datasheet);
                            return new DynamicValue((short)result);
                        }
                    case TypeCode.Int32:
                        checked
                        {
                            decimal result = (left.datasheet % right.datasheet);
                            return new DynamicValue((int)result);
                        }
                    case TypeCode.Int64:
                        checked
                        {
                            decimal result = (left.datasheet % right.datasheet);
                            return new DynamicValue((long)result);
                        }
                    case TypeCode.UInt16:
                        checked
                        {
                            decimal result = (left.datasheet % right.datasheet);
                            return new DynamicValue((ushort)result);
                        }
                    case TypeCode.UInt32:
                        checked
                        {
                            decimal result = (left.datasheet % right.datasheet);
                            return new DynamicValue((uint)result);
                        }
                    case TypeCode.UInt64:
                        checked
                        {
                            decimal result = (left.datasheet % right.datasheet);
                            return new DynamicValue((ulong)result);
                        }
                    case TypeCode.Single:
                        checked
                        {
                            decimal result = (left.datasheet % right.datasheet);
                            return new DynamicValue((float)result);
                        }
                    case TypeCode.Double:
                        checked
                        {
                            decimal result = (left.datasheet % right.datasheet);
                            return new DynamicValue((double)result);
                        }
                    case TypeCode.Decimal:
                        checked
                        {
                            decimal result = (left.datasheet % right.datasheet);
                            return new DynamicValue(result);
                        }
                }
            }
            else throw NewInvalidCastException();
        }
        #endregion
    }
}
