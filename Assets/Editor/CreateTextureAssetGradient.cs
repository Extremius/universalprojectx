﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Experimental
{
    public class CreateTextureAssetGradient : MonoBehaviour
    {
      
        [MenuItem("CreateTextures/Create gray gradient")]
        static void CreateGrayTexture()
        {
            Texture2D texture = new Texture2D(256, 1);

            int x_inc = 0;
            for(int i = 256; i>=0;i--)
            {
                var del = i / 256f;
                texture.SetPixel(x_inc++, 0,
                    new Color(del,del,del,1f));
            }

            texture.Apply();
            AssetDatabase.CreateAsset(texture, "Assets/NewGrayGradientTexture.asset");
        }
    }
}
