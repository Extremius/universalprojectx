﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Terrain;
using Stopwatch = System.Diagnostics.Stopwatch;

public class UnityChunk : MonoBehaviour
{
    public const int chunkSize = RuntimeChunk.chunkSize;
    #region native
    private RuntimeChunk nativeChunk;
    public RuntimeChunk Chunk => nativeChunk;
    #endregion

    [SerializeField] MeshFilter filter;
    [SerializeField] new MeshCollider collider;
    [SerializeField] new MeshRenderer renderer;
    Stopwatch watch = Stopwatch.StartNew();
    Mesh currentMesh;
    //RuntimeBlock[,,] blocks;
    public void SetRuntimeChunk(RuntimeChunk chunk)
    {
        var glsh = WorldGenerator.Self.GlobalShift;

        nativeChunk = chunk;
        Vector3 chunkPos = new Vector3(chunk.ChunkPosition.x * RuntimeChunk.chunkSize,
            chunk.ChunkPosition.y * RuntimeChunk.chunkSize,
            chunk.ChunkPosition.z * RuntimeChunk.chunkSize);

        transform.position = chunkPos - glsh;

        renderer.material = WorldGenerator.Self.MainMaterial;

        
    }

    public void Enable()
    {
        //UpdateMeshData();

        //StartCoroutine(LightUVUpdate());
        isEnabled = true;
    }

    private bool isEnabled = false;

    bool isDestroying = false;
    public void DestroySelf()
    {
        if (!isDestroying)
        {
            isDestroying = true;
            nativeChunk = null;
            gameObject.SetActive(false);
            Destroy(gameObject, Random.Range(0, 1));
        }
    }

    void UpdateMeshData()
    {
        if (currentMesh == null)
        {
            currentMesh = new Mesh();

            filter.mesh = currentMesh;
            filter.sharedMesh = currentMesh;
            collider.sharedMesh = currentMesh;

            
        }
        currentMesh.Clear();
        currentMesh.vertices = nativeChunk.Verts;
        currentMesh.uv = nativeChunk.UV_0;
        currentMesh.triangles = nativeChunk.Tris;
        currentMesh.SetUVs(1, nativeChunk.UV_2);
        currentMesh.RecalculateNormals();

        collider.sharedMesh = currentMesh;
    }

    ulong meshIndex = 0;
    ulong lightIndex = 0;
    IEnumerator LightUVUpdate()
    {
        yield return new WaitForSecondsRealtime(Random.Range(0.1f, 1));
        while (true)
        {
            
            yield return null;
        }
    }

    private void Update()
    {
        if (!isEnabled)
            return;

        if(Chunk.IsDisposed)
        {
            isEnabled = false;
            DestroySelf();
            return;
        }

        if (!Chunk.MeshIsDirty)
        {
            if (Chunk.MeshUpdated(ref meshIndex))
            {
                UpdateMeshData();
                Chunk.LightUpdated(ref lightIndex);

                //watch.Restart();
            }
            else if (Chunk.LightUpdated(ref lightIndex))
            {
                currentMesh.SetUVs(1, Chunk.UV_2);
                Debug.Log($"Light uv in unity was updated in {nativeChunk.ChunkPosition.ToString()}");
                //watch.Restart();
            }
        }
    }

    #region diagnostic
    public void Diagnostic_MeshVertex()
    {
        var verts = currentMesh.vertices;
        int equalsVerts = 0;
        Dictionary<int, int> iTojPair = new Dictionary<int, int>();
        Dictionary<int, int> jToiPair = new Dictionary<int, int>();

        for(int i =0;i<verts.Length;i++)
        for(int j =0;j<verts.Length;j++)
            {
                FindEquals(i, j);
            }

        if (equalsVerts > 0)
        {
            Debug.Log($"Find {equalsVerts} equaled verts from total {verts.Length}. Equal percent = {((float)equalsVerts / verts.Length) * 100:f1}%");
        }
        else
            Debug.Log("No equals verts");

        void FindEquals(int i, int j)
        {
            if(i != j)
            {
                if (!iTojPair.ContainsKey(i))
                    if (!jToiPair.ContainsKey(j))
                        if (verts[i] == verts[j])
                        {
                            iTojPair.Add(i, j);
                            jToiPair.Add(j, i);
                            equalsVerts++;
                        }
            }
        }
    }
    #endregion
}
