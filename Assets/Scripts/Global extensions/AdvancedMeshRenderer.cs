﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;
using System;

public class AdvancedMeshRenderer : MonoBehaviour
{
    #region components
    private interface IShaderParametr
    {
        string shaderParametrName { get; set; }
        void UpdateParametr(Material mat);
    }
    [Serializable] public struct ShaderTexture : IShaderParametr
    {
        [SerializeField]private string _shaderParamName;
        public string shaderParametrName { get => _shaderParamName; set => _shaderParamName = value; }
        public Texture2D texture;

        public void UpdateParametr(Material mat)
        {
            mat.SetTexture(shaderParametrName, texture);
        }
    }
    [Serializable] public struct ShaderColor : IShaderParametr
    {
        [SerializeField] private string _shaderParamName;
        public string shaderParametrName { get => _shaderParamName; set => _shaderParamName = value; }
        [ColorUsage(true, true)] public Color color;

        public void UpdateParametr(Material mat)
        {
            mat.SetColor(shaderParametrName, color);
        }
    }
    #endregion

    private new MeshRenderer renderer;
    public ShaderTexture texture;
    public ShaderColor color;

    private void Awake()
    {
        renderer = GetComponent<MeshRenderer>();
    }

    private void Start()
    {
        UpdateMaterial();
    }

    public void UpdateMaterial()
    {
        if(renderer is null)
            renderer = GetComponent<MeshRenderer>();
        var mat = renderer.material;
        texture.UpdateParametr(mat);
        color.UpdateParametr(mat);
    }
    
}


