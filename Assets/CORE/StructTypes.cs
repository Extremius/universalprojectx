﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Core
{
    public enum Rotate : byte
    {
        North = 0,
        East,
        South,
        West,
        Up,
        Down,
    }
    public struct Vector3Int
    {
        public int x, y, z;

        public double Magnitude => Math.Sqrt(x * x + y * y + z * z);

        public double SqrMagnitude => (double)(x * x + y * y + z * z);

        public Vector3Int(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public Vector3Int(int x, int y) : this(x, y, 0) { }
        public Vector3Int(int x) : this(x, 0, 0) { }

        public static Vector3Int operator +(Vector3Int l, Vector3Int r)
        {
            return new Vector3Int(l.x + r.x, l.y + r.y, l.z + r.z);
        }
        public static Vector3Int operator -(Vector3Int l, Vector3Int r)
        {
            return new Vector3Int(l.x - r.x, l.y - r.y, l.z - r.z);
        }
        public static Vector3Int operator /(Vector3Int l, int r)
        {
            return new Vector3Int(l.x / r, l.y / r, l.z / r);
        }
        public static Vector3Int operator *(Vector3Int l, int r)
        {
            return new Vector3Int(l.x * r, l.y * r, l.z * r);
        }
        public static Vector3Int operator %(Vector3Int l, int r)
        {
            return new Vector3Int(l.x % r, l.y % r, l.z % r);
        }

        public static implicit operator Vector3Long(Vector3Int l)
        {
            return new Vector3Long(l.x, l.y, l.z);
        }
        public static bool operator !=(Vector3Int l, Vector3Int r)
        {
            if (l.x == r.x)
                if (l.y == r.y)
                    if (l.z == r.z)
                        return false;
            return true;
        }

        public static bool operator ==(Vector3Int l, Vector3Int r)
        {
            if (l.x == r.x)
                if (l.y == r.y)
                    if (l.z == r.z)
                        return true;
            return false;
        }
        public override string ToString()
        {
            return $"[{x}; {y}; {z}]";
        }
    }
    public struct Vector3Long
    {
        public long x, y, z;

        public double Magnitude => Math.Sqrt(x * x + y * y + z * z);

        public double SqrMagnitude => (double)(x * x + y * y + z * z);

        public Vector3Long(long x, long y, long z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public Vector3Long(long x, long y) : this(x, y, 0) { }
        public Vector3Long(long x) : this(x, 0, 0) { }

        public static Vector3Long operator +(Vector3Long l, Vector3Long r)
        {
            return new Vector3Long(l.x + r.x, l.y + r.y, l.z + r.z);
        }
        public static Vector3Long operator -(Vector3Long l, Vector3Long r)
        {
            return new Vector3Long(l.x - r.x, l.y - r.y, l.z - r.z);
        }
        public static Vector3Long operator /(Vector3Long l, long r)
        {
            return new Vector3Long(l.x / r, l.y / r, l.z / r);
        }
        public static Vector3Long operator *(Vector3Long l, long r)
        {
            return new Vector3Long(l.x * r, l.y * r, l.z * r);
        }
        public static Vector3Long operator %(Vector3Long l, long r)
        {
            return new Vector3Long(l.x % r, l.y % r, l.z % r);
        }

        public static bool operator !=(Vector3Long l, Vector3Long r)
        {
            if (l.x == r.x)
                if (l.y == r.y)
                    if (l.z == r.z)
                        return false;
            return true;
        }

        public static bool operator ==(Vector3Long l, Vector3Long r)
        {
            if (l.x == r.x)
                if (l.y == r.y)
                    if (l.z == r.z)
                        return true;
            return false;
        }

        public static explicit operator Vector3Int(Vector3Long l)
        {
            return new Vector3Int((int)l.x, (int)l.y, (int)l.z);
        }

        public static implicit operator Vector3(Vector3Long l)
        {
            return new Vector3(l.x, l.y, l.z);
        }

        public static explicit operator Vector3Long(Vector3 l)
        {
            return new Vector3Long((long)l.x, (long)l.y, (long)l.z);
        }

        public override string ToString()
        {
            return $"[{x}; {y}; {z}]";
        }
    }

    public struct Vector3Byte
    {
        public byte x, y, z;

        public double Magnitude => Math.Sqrt(x * x + y * y + z * z);

        public double SqrMagnitude => (double)(x * x + y * y + z * z);

        public Vector3Byte(byte x, byte y, byte z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public Vector3Byte(byte x, byte y) : this(x, y, 0) { }
        public Vector3Byte(byte x) : this(x, 0, 0) { }

        public static Vector3Byte operator +(Vector3Byte l, Vector3Byte r)
        {
            return new Vector3Byte((byte)(l.x + r.x), (byte)(l.y + r.y), (byte)(l.z + r.z));
        }
        public static Vector3Byte operator -(Vector3Byte l, Vector3Byte r)
        {
            return new Vector3Byte((byte)(l.x - r.x), (byte)(l.y - r.y), (byte)(l.z - r.z));
        }
        public static Vector3Byte operator /(Vector3Byte l, int r)
        {
            return new Vector3Byte((byte)(l.x / r), (byte)(l.y / r), (byte)(l.z / r));
        }
        public static Vector3Byte operator *(Vector3Byte l, int r)
        {
            return new Vector3Byte((byte)(l.x * r), (byte)(l.y * r), (byte)(l.z * r));
        }
        public static Vector3Byte operator %(Vector3Byte l, int r)
        {
            return new Vector3Byte((byte)(l.x % r), (byte)(l.y % r), (byte)(l.z % r));
        }

        public static bool operator !=(Vector3Byte l, Vector3Byte r)
        {
            if (l.x == r.x)
                if (l.y == r.y)
                    if (l.z == r.z)
                        return false;
            return true;
        }

        public static bool operator ==(Vector3Byte l, Vector3Byte r)
        {
            if (l.x == r.x)
                if (l.y == r.y)
                    if (l.z == r.z)
                        return true;
            return false;
        }

        public static implicit operator Vector3Int(Vector3Byte l)
        {
            return new Vector3Int(l.x, l.y, l.z);
        }
        public static implicit operator Vector3Long(Vector3Byte l)
        {
            return new Vector3Long(l.x, l.y, l.z);
        }
        public static explicit operator Vector3Byte(Vector3Int l)
        {
            return new Vector3Byte((byte)l.x, (byte)l.y, (byte)l.z);
        }
        public static explicit operator Vector3Byte(Vector3Long l)
        {
            return new Vector3Byte((byte)l.x, (byte)l.y, (byte)l.z);
        }

        public static explicit operator Vector3Byte(Vector3 l)
        {
            return new Vector3Byte((byte)l.x, (byte)l.y, (byte)l.z);
        }

        public override string ToString()
        {
            return $"[{x}; {y}; {z}]";
        }
    }
   
    /// <summary>
    /// one byte floating number from 0 to 1 without overfloat. Step = 0.392%
    /// </summary>
    public readonly struct USingleHalf
    {
        private const float fromByteToFloat = 1f/byte.MaxValue;
        private const double fromByteToDouble = 1d/byte.MaxValue;

        public const float MinValue = 0;
        public const float MaxValue = 1;

        private readonly byte storedValue;

        private USingleHalf(byte baseVal)
        {
            storedValue = baseVal;
        }

        public static USingleHalf FromRaw(byte rawValue) => new USingleHalf(rawValue);

        public float ToSingle => storedValue * fromByteToFloat;
        public byte ToByte => storedValue;

        public static implicit operator float(USingleHalf l)
        {
            return l.storedValue * fromByteToFloat;
        }
        public static implicit operator double(USingleHalf l)
        {
            return l.storedValue * fromByteToDouble;
        }

        public static explicit operator USingleHalf(float l)
        {
            if (l > 1f) l = 1f;
            else if (l < 0) l = 0;

            return new USingleHalf((byte)(l * byte.MaxValue));
        }
        public static explicit operator USingleHalf(double l)
        {
            if (l > 1d) l = 1d;
            else if (l < 0) l = 0;

            return new USingleHalf((byte)(l * byte.MaxValue));
        }

        public static USingleHalf operator +(USingleHalf l, USingleHalf r)
        {
            int summary = l.storedValue + r.storedValue;
            if (summary > byte.MaxValue)
                return new USingleHalf(byte.MaxValue);
            else return new USingleHalf((byte)summary);
        }

        public static USingleHalf operator -(USingleHalf l, USingleHalf r)
        {
            int summary = l.storedValue * r.storedValue;
            if (summary < byte.MinValue)
                return new USingleHalf(byte.MinValue);
            else return new USingleHalf((byte)summary);
        }

        public static USingleHalf operator *(USingleHalf l, USingleHalf r)
        {
            var res = l.ToSingle * r.ToSingle;
            return (USingleHalf)res;
        }
        public static USingleHalf operator /(USingleHalf l, USingleHalf r)
        {
            var res = l.ToSingle / r.ToSingle;
            return (USingleHalf)res;
        }

        public static bool operator !=(USingleHalf l, USingleHalf r)
        {
            return l.storedValue != r.storedValue;
        }
        public static bool operator ==(USingleHalf l, USingleHalf r)
        {
            return l.storedValue == r.storedValue;
        }
        public static bool operator >(USingleHalf l, USingleHalf r)
        {
            return l.storedValue > r.storedValue;
        }
        public static bool operator <(USingleHalf l, USingleHalf r)
        {
            return l.storedValue < r.storedValue;
        }
        public static bool operator >=(USingleHalf l, USingleHalf r)
        {
            return l.storedValue >= r.storedValue;
        }
        public static bool operator <=(USingleHalf l, USingleHalf r)
        {
            return l.storedValue <= r.storedValue;
        }

        public override string ToString()
        {
            return this.ToSingle.ToString();
        }
        public override int GetHashCode()
        {
            return storedValue.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj is USingleHalf h)
            {
                return this == h;
            }
            else return false;
        }
    }

    

    public struct Vector2USH
    {
        public USingleHalf x, y;

        public double Magnitude => Math.Sqrt(x * x + y * y );

        public double SqrMagnitude => x * x + y * y;

        public Vector2USH(USingleHalf x, USingleHalf y)
        {
            this.x = x;
            this.y = y;
        }
        public Vector2USH(float x, float y)
        {
            this.x = (USingleHalf)x;
            this.y = (USingleHalf)y;
        }
    }
}
