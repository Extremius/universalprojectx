﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Terrain
{
    public class MeshBuilderThread : IRunnable
    {
        public bool IsRunning => isRunning;
        private bool isRunning = false;
        private bool isPaused = false;
        private bool collectionWasModifier = false;
        Thread[] threads;
        Thread nearblyThread;
        IList<IMeshBuildSupport> nearblyMeshes;
        List<IMeshBuildSupport> meshes = new List<IMeshBuildSupport>();
        TimeSpan delay = new TimeSpan(1_00_000); //10 ms
        private readonly object _sync = new object();
        Semaphore _pool = new Semaphore(0,2);

        void RunNearThread()
        {
            if (isRunning)
            {
                nearblyThread = new Thread(near_loop);
                nearblyThread.Name = "Nearbly Mesh builder";
                nearblyThread.Priority = ThreadPriority.AboveNormal;
                nearblyThread.Start();
            }
        }
        public void SetNearblyeMeshes(IList<IMeshBuildSupport> list)
        {
            nearblyMeshes = list;
            if(nearblyMeshes!=null)
            RunNearThread();
        }

        public void Add(IMeshBuildSupport item)
        {
            if (!meshes.Contains(item))
            {
                meshes.Add(item);
                collectionWasModifier = true;
            }
        }
        public bool Remove(IMeshBuildSupport item)
        {
            if (meshes.Remove(item))
            {
                collectionWasModifier = true;
                return true;
            }
            else return false;
        }

        public void Execute()
        {
            if (isRunning)
                throw new InvalidOperationException("Thread already running");

            isIdle = new bool[2];
            isRunning = true;

            threads = new Thread[2];

            for(int i =0;i<threads.Length;i++)
            {
                threads[i] = new Thread(loop_pass);
                threads[i].Name = $"Mesh Builder thread {i}";
                threads[i].Start((i, threads.Length));
            }

            if(nearblyMeshes != null)
            {
                RunNearThread();
            }
        }

        public void Terminate()
        {
            if(isRunning)
            {
                isRunning = false;

                foreach (var th in threads)
                    th.Join(1000);
                nearblyThread?.Join(1000);
            }
        }

        public bool IsIdle
        {
            get
            {
                foreach (var bl in isIdle)
                    if (!bl)
                        return false;
                return true;
            }
        }
        bool[] isIdle;
        private float progress = 0;
        public float Progress => progress;

        private void loop_pass(object args)
        {
            (int index, int passing) arg = ((int,int))args;

            while (isRunning)
            {
                //_pool.WaitOne();

                var pass = 1f / meshes.Count;
                progress = 0;
                for (int i = arg.index; i < meshes.Count; i += arg.passing)
                {
                    var itm = meshes[i];
                    if (itm != null)
                    {
                        if (itm.IsDirty && !itm.IsDisposed)
                        {
                            //itm.BuildMesh();
                            if(itm!=null)
                                RebuildMeshTask(itm);
                        }
                    }
                    progress += pass;

                    if (collectionWasModifier)
                    {
                        collectionWasModifier = false;
                        break;
                    }
                }

                //_pool.Release();


                    isIdle[arg.index] = true;
                    Thread.Sleep(delay);
                    isIdle[arg.index] = false;

            }
        }

        List<IMeshBuildSupport> reqUpdateNearMeshes = new List<IMeshBuildSupport>(27);
        void near_loop()
        {
            while (isRunning)
            {
                if(nearblyMeshes is null)
                {
                    Thread.Sleep(delay);
                    continue;
                }
                int count = 0;
                lock (nearblyMeshes)
                {
                    //reqUpdateNearMeshes.Clear();
                    
                    foreach(var itm in nearblyMeshes)
                    {
                        if (!itm.IsDisposed)
                            if (itm.IsDirty)
                                if (!itm.IsBuilding)
                                {
                                    //reqUpdateNearMeshes.Add(itm);
                                    itm.BuildMesh();
                                }
                    }
                }
                
                if(count is 0)
                    Thread.Sleep(delay);
                //if (reqUpdateNearMeshes.Count > 0)
                //{
                //    skipSpecialMeshes = 0;
                //    foreach (var itm in reqUpdateNearMeshes)
                //    {
                //        if (!itm.IsDisposed)
                //            itm.BuildMesh();
                //    }
                //}
                //else

            }
        }

        int skipSpecialMeshes = 0;
        void RebuildMeshTask(IMeshBuildSupport itm)
        {
            if (!itm.IsDisposed)
                if (itm.IsDirty)
                    if (!itm.IsBuilding)
                    {
                        if (skipSpecialMeshes < 27)
                        {
                            if (nearblyMeshes != null)
                                if (nearblyMeshes.Contains(itm))
                                {
                                    skipSpecialMeshes++;
                                    return;
                                }
                        }

                        itm.BuildMesh();
                    }
            
        }
        

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты).
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                Terminate();
                // TODO: задать большим полям значение NULL.

                disposedValue = true;
            }
        }

        //TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        ~MeshBuilderThread()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(false);
        }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
           GC.SuppressFinalize(this);
        }
        #endregion
    }

    public interface IMeshBuildSupport
    {
        bool IsBuilding { get; }
        bool IsDirty { get; }
        bool IsDisposed { get; }
        void BuildMesh();
        void SetDirty();
    }
}
