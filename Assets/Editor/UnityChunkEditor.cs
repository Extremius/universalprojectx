﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UnityChunk))]
public class UnityChunkEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if(GUILayout.Button("Inspect mesh"))
        {
            (target as UnityChunk).Diagnostic_MeshVertex();
        }
    }
}
