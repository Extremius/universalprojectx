﻿/*
 * помойка? unity physics выручает
 * не оправдал себя -_-
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Core.Terrain;

namespace Core.Light
{
    public struct LightRay
    {
        public const float deltaDistance = 1f;
        private static readonly Vector3 _lforward = Vector3.forward;
        private const float maxRayDistance = 256;

        public static readonly int reqLayer = LayerMask.GetMask("Terrain");

        Vector3 selfPosition;
        Quaternion direction;
        USingleHalf lightIntensity;
        private byte bouncedCount;
        private ushort lightID;
        public float distance;
        #region cash
        Vector3Long globalIndex;
        RuntimeBlock block;
        Vector3 endPoint;
        float passedDistance;

        public Vector3 EndPoint => endPoint;
        public float PassedDistance => passedDistance;

        public LightRay(Vector3 selfPosition, Quaternion direction, float lightIntensity, byte bouncedCount, float distance) : this()
        {
            this.selfPosition = selfPosition;
            this.direction = direction;
            this.lightIntensity = (USingleHalf)(lightIntensity *0.01f);
            this.bouncedCount = bouncedCount;
            this.distance = distance;
        }
        #endregion

        #region behaviour
        public void Cast(List<Vector3Long> chunksPos, ushort lightID)
        {
            this.lightID = lightID;
            TraceRay(chunksPos);
        }
        private void TraceRay(List<Vector3Long> chunkPos)
        {
            passedDistance = 0;
            SelfWorld world = WorldGenerator.Self.World;
            //const float distance = 256;
            Vector3 dir = direction * _lforward; //trace direction
            var ray = new Ray(selfPosition, dir);
            RaycastHit? hit;
            var globalShift = WorldGenerator.Self.GlobalShift;
            var localShift = Vector3.one * 0.5f;
            float reverseDistance = 1f / (distance * bouncedCount);

            do
            {
                //debug
                //Debug.DrawRay(ray.Point, dir * distance, Color.blue, 10f);

                hit = Trace(ref ray, distance);
                Debug.DrawLine(ray.BeginPoint, ray.Point, Color.blue, 15f);
                //Debug.DrawRay(ray.Point, ray.Direction * 0.02f, Color.yellow, 15f);
                passedDistance += ray.PassedDistance;
                if (hit.HasValue)
                {
                    if (block.typeReference is 0)
                        Debug.Log("Null block type was get");
                    bouncedCount--;
                    var rayHit = hit.Value;
                    //var block = hit.Value;
                    var light = block.Light;

                    switch (light.settings)
                    {
                        case LightSettings.Transparent:
                            {
                                light.SetLocalIntensity(lightIntensity, lightID);

                                lightIntensity *= light.reflectionLight;
                               
                            }
                            goto NEXT;
                        case LightSettings.None:
                            {
                                //light update
                                //Vector3 normal = light.GetLightNormal(ray.Point - (globalIndex + globalShift + localShift));
                                dir = Vector3.Reflect(dir, rayHit.normal);
                                var side = light.GetSideFromNormal(rayHit.normal);

                                //mk2
                                //var trueIntensity = (USingleHalf)(lightIntensity * (1f - passedDistance * reverseDistance));
                                //side.SetLocalIntensity(trueIntensity, lightID);

                                //mk1
                                side.SetLocalIntensity(lightIntensity, lightID);
                                lightIntensity *= light.reflectionLight;
                                light.SetSideFromNormal(rayHit.normal, side);   
                            }
                            goto NEXT;
                        case LightSettings.Void:
                            goto END;
                    }

                NEXT:
                    {
                        //block.Light = light;
                        //ray = new Ray(ray.Point + dir, dir);
                        ray = new Ray(ray.Point, dir);

                        var chP = WorldGenerator.GetChunkPos(globalIndex);
                        ref RuntimeBlock refBlock = ref (world.Chunks[chP] as IUnsafeReadWriteDirectlyBlocksData).ReadRef(block.localPosition);
                        refBlock.Light = light;
                        if (!chunkPos.Contains(chP))
                            chunkPos.Add(chP);

                        continue;
                    }
                }
                else goto END; //end trace
            }
            while (bouncedCount > 0);

            END:
            {
                endPoint = ray.Point;
                passedDistance = ray.PassedDistance;
            }
        }

        RaycastHit? Trace(ref Ray ray, float distance)
        {
            if (distance < 0)
                throw new ArgumentOutOfRangeException("Дистанция меньше 0");

            SelfWorld world = WorldGenerator.Self.World;
            //Vector3Long globalIndex;
            Vector3 globalShift = WorldGenerator.Self.GlobalShift;
            //RuntimeBlock block;

            #region mk4
            //using unity physics
            if(Physics.Raycast(ray.BeginPoint, ray.Direction, out var hitInfo, distance, reqLayer, QueryTriggerInteraction.Collide))
            {
                ray = new Ray(ray.BeginPoint, ray.Direction, hitInfo);
                globalIndex = (Vector3Long)(hitInfo.point - hitInfo.normal*0.5f);
                block = world.ReadBlockUnsafe(globalIndex);
                return hitInfo;
            }
            return null;
            #endregion

            //do
            //{
            //    globalIndex = (Vector3Long)(ray.Step() + globalShift);

            //    block = world.ReadBlockUnsafe(globalIndex);
            //    if (block.typeReference is 0)
            //        continue;
            //    else
            //    {
                    

            //        #region mk3
            //        //ray.Back();
            //        var hitedPoint = CubeIntersect(ray, globalIndex);
            //        ray = Ray.EndRay(ray, hitedPoint);
            //        return block;
            //        #endregion

            //        #region mk2
            //        //return block;

            //        //var deltaVec = ray.Point - globalIndex;
            //        //ray.Back(deltaVec.magnitude);
            //        //return block;
            //        #endregion

            //        #region mk1

            //        //ray.Back();
            //        //Vector3Long localIndex;
            //        //const float ddeltaDis = deltaDistance * 0.002f;
            //        //do
            //        //{
            //        //    localIndex = (Vector3Long)(ray.Step(ddeltaDis) + globalShift);
            //        //    if (ray.PassedDistance > maxRayDistance)
            //        //    {
            //        //        Debug.DrawRay(ray.BeginPoint, ray.Direction * ray.PassedDistance, Color.blue, 15f);
            //        //        Debug.Log("Infinity ray detected");
            //        //        break;
            //        //    }
            //        //} while (globalIndex != localIndex);
            //        //return block;
            //        #endregion
            //    }
            //}
            //while (ray.PassedDistance<=distance);

            //return null;
        }

        Vector3 CubeIntersect(Ray ray, Vector3Long cubeIndex)
        {
            Vector3 CUBE_MIN = cubeIndex;
            Vector3 CUBE_MAX = cubeIndex + Vector3.one;

            Vector3 tMin, tMax, t1, t2, i;
            float tNear;

            tMin = (CUBE_MIN - ray.BeginPoint).DIV(ray.Direction);
            tMax = (CUBE_MAX - ray.BeginPoint).DIV(ray.Direction);
            t1 = Vector3.Min(tMin, tMax);
            t2 = Vector3.Max(tMin, tMax);
            tNear = Mathf.Max(Mathf.Max(t1.x, t1.y), t1.z);
            //float tFar = Mathf.Min(Mathf.Min(t2.x, t2.y), t2.z);
            tNear = tNear <= 0 ? Mathf.Min(Mathf.Min(t2.x, t2.y), t2.z) : 0;
            i = ray.Direction;
            i *= tNear;
            return ray.BeginPoint + i;

            //return ray.BeginPoint + ray.Direction * tNear;
        }
        
        #endregion

        private struct Ray
        {
            private readonly Vector3 startRayPosition; 
            private Vector3 rayPosition;
            private readonly Vector3 direction;

            public Vector3 Direction => direction;
            public Vector3 BeginPoint => startRayPosition;
            public Vector3 Point => rayPosition;
            public float PassedDistance => passedDistance;
            private float passedDistance;
            public Ray(Vector3 rayPosition, Vector3 direction)
            {
                startRayPosition = this.rayPosition = rayPosition;
                this.direction = direction;
                this.passedDistance = 0;
            }
            public Ray(Vector3 rayPosition, Vector3 direction, RaycastHit fromHit)
            {
                startRayPosition = rayPosition;
                this.rayPosition = fromHit.point;
                passedDistance = fromHit.distance;
                this.direction = direction;
            }
            public static Ray EndRay(in Ray refRay, Vector3 endPoint)
            {
                Ray newRay = new Ray(refRay.startRayPosition, refRay.direction);
                newRay.rayPosition = endPoint;
                newRay.passedDistance = (newRay.startRayPosition - endPoint).magnitude;
                return newRay;
            }
            public Vector3 Step()
            {
                rayPosition += direction * deltaDistance;
                passedDistance += deltaDistance;
                return rayPosition;
            }
            public Vector3 Step(float delta)
            {
                rayPosition += direction * delta;
                passedDistance += delta;
                return rayPosition;
            }

            public Vector3 Back()
            {
                rayPosition -= direction * deltaDistance;
                passedDistance -= deltaDistance;
                return rayPosition;
            }

            public Vector3 Back(float delta)
            {
                rayPosition -= direction * delta;
                passedDistance -= delta;
                return rayPosition;
            }
        }
    }

    public static class Vector3Utils
    {
        public static Vector3 DIV(this Vector3 self, Vector3 divided)
        {
            Vector3 res;
            res.x = self.x / divided.x;
            res.y = self.y / divided.y;
            res.z = self.z / divided.z;
            return res;
        }
    }
}
