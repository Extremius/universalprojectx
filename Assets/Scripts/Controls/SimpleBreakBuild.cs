﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;
using Core.Terrain;

public class SimpleBreakBuild : MonoBehaviour
{
    public GameObject lightPref;
    public GameObject goPoint;
    public Transform cam;
    public float distance = 5;

    RaycastHit[] hits = new RaycastHit[16];
    int raycastHitsCount = 0;
    Vector3Long point = default;
    bool isFind = false;
    RaycastHit thisHit = default;

    private void Update()
    {
        Castray();
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            if(isFind)
            {
                Vector3Long glPos = (Vector3Long)(point);
                if (thisHit.normal.x > 0)
                    glPos.x -= 1;
                if (thisHit.normal.y > 0)
                    glPos.y -= 1;
                if (thisHit.normal.z > 0)
                    glPos.z -= 1;

                if (point.x < 0)
                    glPos.x += 1;
                if (point.y < 0)
                    glPos.y += 1;
                if (point.z < 0)
                    glPos.z += 1;

                var blck = WorldGenerator.Self.ReadBlock(glPos);
                if(blck.HasValue)
                {
                    var val = blck.Value;
                    val.typeReference = 0;
                    WorldGenerator.Self.FillBlock(WorldGenerator.GetChunkPos(glPos), val);
                }
            }
        }
        else if(Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (isFind)
            {
                Vector3Long glPos = (Vector3Long)(point + thisHit.normal);
                if (thisHit.normal.x > 0)
                    glPos.x -= 1;
                if (thisHit.normal.y > 0)
                    glPos.y -= 1;
                if (thisHit.normal.z > 0)
                    glPos.z -= 1;

                if (point.x < 0)
                    glPos.x += 1;
                if (point.y < 0)
                    glPos.y += 1;
                if (point.z < 0)
                    glPos.z += 1;

                var blck = WorldGenerator.Self.ReadBlock(glPos);
                if (blck.HasValue)
                {
                    //LightModel mdl = new LightModel(100, new Vector2USH(0, 0), 0.9f);
                    RuntimeBlock rtlBlck = blck.Value;
                    rtlBlck.typeReference = 1;
                    WorldGenerator.Self.FillBlock(WorldGenerator.GetChunkPos(glPos), rtlBlck);
                }
            }
        }
        else if(Input.GetKeyDown(KeyCode.T))
        {
            if(isFind)
            {
                Vector3Long glPos = (Vector3Long)(point + thisHit.normal);
                if (thisHit.normal.x > 0)
                    glPos.x -= 1;
                if (thisHit.normal.y > 0)
                    glPos.y -= 1;
                if (thisHit.normal.z > 0)
                    glPos.z -= 1;

                var light = Instantiate(lightPref);
                light.transform.position = glPos + Vector3.one * 0.5f;
            }
        }
    }

    void Castray()
    {
        Ray ray = new Ray(cam.position, cam.forward);
        raycastHitsCount = Physics.RaycastNonAlloc(ray, hits, distance);
        isFind = false;
       
        for (int i = 0; i < raycastHitsCount; i++)
        {
            if (hits[i].collider.isTrigger)
                continue;

            if (hits[i].collider.CompareTag("Terrain"))
            {
                isFind = true;
                point = (Vector3Long)hits[i].point;
                thisHit = hits[i];
                break;
            }
        }

        if (!isFind)
        {
            goPoint.transform.position = Vector3.zero;
            goPoint.SetActive(false);
            return;
        }
        else
        {
            goPoint.SetActive(true);
            Vector3 offset = new Vector3(0.5f, 0.5f, 0.5f);

            var normal = thisHit.normal;
            var fixedVector = new Vector3();

            if (normal.x != 0)
                offset.x = 0;
            if (normal.y != 0)
                offset.y = 0;
            if (normal.z != 0)
                offset.z = 0;

            fixedVector = Vector3.one * 0.0001f;
            point = (Vector3Long)(thisHit.point + fixedVector);

            if (thisHit.point.x < 0)
                point.x -= 1;
            if (thisHit.point.y < 0)
                point.y -= 1;
            if (thisHit.point.z < 0)
                point.z -= 1;

            offset -= thisHit.normal *0.4999f;

            goPoint.transform.position = point + offset;
            Debug.DrawRay(thisHit.point, thisHit.normal, Color.red, 0.2f);
            
            
        }
    }
}
