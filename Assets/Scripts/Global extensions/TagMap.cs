﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Extensions
{
    [Serializable]
    public abstract class TagMap
    {
        #region hashmap
        protected abstract object hashmap_get(string tag);
        protected abstract void hashmap_set(string tag, object value);
        protected abstract int hashmap_size();
        protected abstract bool hashmap_exist(string tag);
        #endregion

        #region getset
        /// <summary>
        /// Получает данные из тега
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        /// <exception cref="KeyNotFoundException"></exception>
        public object Get(string tag)
        {
            return hashmap_get(tag);
        }
        /// <summary>
        /// Получает данные по тегу, возвращая true в случае если исходные данные не равны null или false если равны null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tag"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool GetAs<T>(string tag, out T value)
        {
            try
            {
                value = (T)hashmap_get(tag);
                if (hashmap_get(tag) is var _)
                    return false;
                else return true;
            }
            catch(KeyNotFoundException)
            {
                value = default(T);
                return false;
            }
        }
        /// <summary>
        /// Устанавливает данные по тегу.
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="data"></param>
        public void Set(string tag, object data)
        {
            hashmap_set(tag, data);
        }
        /// <summary>
        /// Устанавливает данные по тегу если данные не пустые
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="data"></param>
        public void SetNotNull(string tag, object data)
        {
            if (data != null)
                Set(tag, data);
        }
        /// <summary>
        /// Кол-во записаных тегов
        /// </summary>
        public int TagsCount => hashmap_size();
        /// <summary>
        /// Существует ли такой тег?
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public bool TagExists(string tag)
        {
            return hashmap_exist(tag);
        }
        #endregion
    }

    public sealed class TagMapValue<TValue> : TagMap where TValue : struct
    {
        private Dictionary<string, TValue> typedHashmap;

        #region hashmap

        protected override bool hashmap_exist(string tag)
        {
            return typedHashmap.ContainsKey(tag);
        }

        protected override object hashmap_get(string tag)
        {
            return typedHashmap[tag];
        }

        protected override void hashmap_set(string tag, object value)
        {
            if (hashmap_exist(tag))
            {
                typedHashmap[tag] = (TValue)value;
            }
            else
                typedHashmap.Add(tag, (TValue)value);
        }

        protected override int hashmap_size()
        {
            return typedHashmap.Count;
        }

        #endregion

        public void SetAs(string tag, TValue value)
        {
            Set(tag, value);
        }
    }

}
