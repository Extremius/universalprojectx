﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Terrain;
using System.Threading;
using System;

namespace Core.Light
{
    public class LightThread : IRunnable
    {
        List<ILightDependet> allLightsDependets = new List<ILightDependet>();
        bool isExecuted = false;

        private TimeSpan _rawDelay = new TimeSpan(10_000_000); //1000 ms
        Thread selfThread;

        private readonly object _sync = new object();
        public void Add(ILightDependet item)
        {
            if (allLightsDependets.Contains(item))
                return;
            lock (_sync)
                allLightsDependets.Add(item);
        }
        public bool Remove(ILightDependet item)
        {
            
                return allLightsDependets.Remove(item);
        }

        public void Execute()
        {
            if (isExecuted)
                throw new InvalidOperationException("Thread already running");

            selfThread = new Thread(loop);
            isExecuted = true;
            selfThread.Name = "Light Thread";
            selfThread.Start();
        }
        public bool IsRunning => isExecuted;
        public void Terminate()
        {
            if (isExecuted)
            {
                isExecuted = false;
                selfThread?.Join(5000);
            }
        }

        void loop()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            while(isExecuted)
            {
                lock (_sync)
                {
                    foreach (var lght in allLightsDependets)
                    {
                        try
                        {
                            if(lght.RequestUpdate())
                                lght.UpdateLight();
                        }
                        catch (Exception e)
                        {
                            Debug.LogException(e);
                        }
                    }
                }
                
                var deltaTicks = watch.Elapsed.Ticks - _rawDelay.Ticks;
                watch.Restart();
                if (deltaTicks > 0)
                    continue;
                else
                    Thread.Sleep(new TimeSpan(-deltaTicks));
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты).
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                Terminate();
                // TODO: задать большим полям значение NULL.
                Debug.Log("Light Thread is disposed");
                disposedValue = true;
            }
        }

        // TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        ~LightThread()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(false);
        }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            GC.SuppressFinalize(this);
        }
        #endregion
    }

    public interface ILightDependet
    {
        void UpdateLight();

        bool RequestUpdate();
    }
}

namespace Core
{
    public interface IRunnable : IDisposable
    {
        void Execute();
        void Terminate();

        bool IsRunning { get; }
    }
}
