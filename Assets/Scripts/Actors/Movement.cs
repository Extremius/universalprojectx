﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;
using Core.Terrain;
using Core;

namespace Actors
{ 
public class Movement : MonoBehaviour
{
        #region constants
        public const float minSpeed = 1, maxSpeed = 100;
        public const string moveAxis_X = "Horizontal";
        public const string moveAxis_Y = "Vertical";
        public const string jumpAxis = "Jump";
        public const string ShiftAxis = "Fire3";
        #endregion

        #region fields
        [SerializeField] Vector3 inputDirection;
        [SerializeField] float speed = 5;
        [SerializeField] Rigidbody rb;
        [SerializeField] Controls.CameraControl cam;
        #endregion
        
        #region unity
        private void FixedUpdate()
        {
            //TEMP
            inputDirection.x = Input.GetAxis(moveAxis_X);
            inputDirection.z = Input.GetAxis(moveAxis_Y);

            if(inputDirection.sqrMagnitude != 0)
                rb.AddRelativeForce(inputDirection * speed, ForceMode.Impulse);

            var vec3Rot = cam.transform.rotation.eulerAngles;
            vec3Rot.x = 0; vec3Rot.z = 0;
            transform.rotation = Quaternion.Euler(vec3Rot);

            if(Input.GetKeyDown(KeyCode.Space))
            {
                rb.AddForce(Vector3.up *9, ForceMode.Impulse);
            }

            
            SetNewPosition();
        }

        Vector3Long chunkPos;
        void SetNewPosition()
        {
            var newPos = WorldGenerator.GetChunkPos((Vector3Long)transform.position);
            if(newPos != chunkPos)
            {
                chunkPos = newPos;
                WorldGenerator.Self.World.PlayerChunkPosition = newPos;
            }
        }
        #endregion
    }
}

