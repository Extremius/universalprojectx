﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrain
{
    public class CloudManager : MonoBehaviour
    {
        [SerializeField] Transform followTo;
        void FixedUpdate()
        {
            var pos = followTo.position;
            pos.y = transform.position.y;
            transform.position = pos;
        }
    }
}
