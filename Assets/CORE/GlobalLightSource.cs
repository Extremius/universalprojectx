﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Terrain;
using System;
using System.Threading.Tasks;

namespace Core.Light
{
    public class GlobalLightSource : ILightDependet
    {
        private readonly object _readSync;
        private float globalIntensity = 100;
        public float GlobalIntensity => globalIntensity;
        private float mod = -0.1f;
        private readonly IReadOnlyDictionary<Vector3Long, RuntimeChunk> ireadonlychunkcollection;
        void ILightDependet.UpdateLight()
        {
            //globalIntensity += mod * (float)deltaTime;
            //if(globalIntensity>100)
            //{
            //    globalIntensity = 100;
            //    mod *= -1;
            //}
            //else if(globalIntensity<0)
            //{
            //    globalIntensity = 0;
            //    mod *= -1;
            //}
            //Debug.Log($"GI: {globalIntensity:f0}");

            if (!updateTaskIsRunning)
                UpdateLightModel();
        }
        bool ILightDependet.RequestUpdate() => false;

        private bool updateTaskIsRunning = false;
        async void UpdateLightModel()
        {
            var gli = globalIntensity;
            updateTaskIsRunning = true;

            await Task.Run(delegate {
                try
                {
                    foreach (var pair in ireadonlychunkcollection)
                    {
                        var blocks = pair.Value.VisibleBlocks();
                        var chunk = pair.Value as IUnsafeReadWriteDirectlyBlocksData;

                        foreach (var pos in blocks)
                        {
                            //var blck = chunk.Read(pos);
                            //var mdl = blck.Light;
                            //mdl.SetGlobalIntensity(gli);
                            //chunk.ReadRef(pos).Light = mdl;

                            var refBlck = chunk.ReadRef(pos);
                            var mdl = refBlck.Light;

                            mdl.SetGlobalIntensity(gli);
                            refBlck.Light = mdl;
                        }
                    }
                }
                catch (Exception)
                {
                    return;
                }
                finally
                {
                    updateTaskIsRunning = false;
                }
            });
        }

        public GlobalLightSource(IReadOnlyDictionary<Vector3Long, RuntimeChunk> ircc, object readSync)
        {
            _readSync = readSync;
            WorldGenerator.Self.GetRunnable<LightThread>().Add(this);
            ireadonlychunkcollection = ircc;
        }
    }
}
