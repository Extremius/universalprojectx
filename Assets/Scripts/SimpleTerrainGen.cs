﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTerrainGen : MonoBehaviour
{
    public GameObject pref;

    private void Start()
    {
        for (int x = 0; x < 6; x++)
            for (int z = 0; z < 6; z++)
            {
                var inst = Instantiate(pref);
                inst.transform.position = new Vector3(32 * x, 0, 32 * z);
            }
    }
}
