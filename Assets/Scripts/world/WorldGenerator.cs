﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Terrain;
using Core.Light;
using Vector3Int = Core.Vector3Int;
using System.Threading.Tasks;
using Stopwatch = System.Diagnostics.Stopwatch;
using Core;
using TMPro;

public class WorldGenerator : MonoBehaviour
{
    #region Singleton
    private static WorldGenerator _self;
    public static WorldGenerator Self => _self;

    private void Awake()
    {
        if (_self == null)
        {
            _self = this;
            Init();
        }
        else if (_self != this)
            Destroy(gameObject);
    }

    public void WriteNewData(string dataName, uint typeRef)
    {
        textureDataSheet.WriteNewBlockData(dataName, typeRef);
        typeMapping.WriteNewReference(dataName, typeRef);
    }
    private void Init()
    {
        typeMapping = new BlocksTypeMapping();
        textureDataSheet = new TextureDataSheet(new Vector2(16, 16), atlas);

        WriteNewData("grass", 1);
        WriteNewData("stone", 2);
        WriteNewData("crystal ore", 3);

        allIRunnables = new List<Core.IRunnable>()
        {
            new LightThread(),
            new MeshBuilderThread(),
        };

        foreach (var irun in allIRunnables)
            if (irun is MeshBuilderThread)
                continue;
            else
                irun.Execute();
    }

    private void Start()
    {
        selfWorld = new SelfWorld(new Vector3Long(0, 3, 0));
        allIRunnables.Add(selfWorld);

        globalLightSource = new GlobalLightSource(selfWorld.Chunks, selfWorld._ChunkCollectionSync);

        selfWorld.Execute();
        //GetRunnable<MeshBuilderThread>().Execute();
        StartCoroutine(StartGenerateWorld());
    }

    IEnumerator StartGenerateWorld()
    {
        var watch = Stopwatch.StartNew();
        selfPlayer.GetComponent<Rigidbody>().useGravity = false;

        while(!selfWorld.ChunksUpdated(ref chunksUpdated))
        {
            printProgress.text = $"Генерация мира... {selfWorld.Progress*100:f2}%";
            yield return null;
        }

        //end generation
        selfPlayer.transform.position = movePlayerToPoint.position;
        enableUpdate = true;
        meshBuilder = GetRunnable<MeshBuilderThread>();
        meshBuilder.SetNearblyeMeshes(selfWorld.NearChunks);
        meshBuilder.Execute();
        UpdateUnityChunks();
        Debug.Log("End native generation");

        while (!meshBuilder.IsIdle)
        {
            printProgress.text = $"Создание ландшафта... {meshBuilder.Progress*100:f2}%";
            yield return null;
        }

        watch.Stop();
        Debug.Log("End unity generation");
        Debug.Log($"Generation complete in {watch.Elapsed.TotalSeconds:f2} s");
        printProgress.text = $"Завершено за {watch.Elapsed.TotalSeconds:f2} сек";
        
        selfPlayer.GetComponent<Rigidbody>().useGravity = true;
    }
    MeshBuilderThread meshBuilder;
    private bool enableUpdate = false;
    private void Update()
    {
        if (!enableUpdate)
            return;

        if(selfWorld.ChunksUpdated(ref chunksUpdated))
        {
            UpdateUnityChunks();
        }

        //if (!meshBuilder.IsIdle)
        //    printProgress.text = $"Rebuild mesh {meshBuilder.Progress * 100:f2}%";
        //else
        //    printProgress.text = "mesh rebuilded";
    }

    void UpdateUnityChunks()
    {
        lock(selfWorld._ChunkCollectionSync)
        {
            var collection = selfWorld.Chunks;

            //add new chunks
            foreach (var pos in selfWorld.RequestToLoad)
            {
                var newChunk = Instantiate(prefChunk);
                newChunk.transform.SetParent(this.transform);
                newChunk.SetRuntimeChunk(collection[pos]);
                newChunk.Enable();
                builtInChunks.Add(pos, newChunk);
            }

            //remove chunks
            foreach(var pos in selfWorld.RequestToUnload)
            {
                if(builtInChunks.TryGetValue(pos, out var oldChunk))
                {
                    builtInChunks.Remove(pos);
                    oldChunk.DestroySelf();
                }
            }
        }
    }
    #endregion

    #region info
    [SerializeField] TextMeshProUGUI printProgress;
    [SerializeField] Transform movePlayerToPoint;
    [SerializeField] Transform selfPlayer;
    #endregion

    #region fields
    public Texture2D Atlas => atlas;
    [SerializeField] Texture2D atlas;

    public Material MainMaterial => mainMaterial;
    [SerializeField] Material mainMaterial;

    public TextureDataSheet TDS => textureDataSheet;
    TextureDataSheet textureDataSheet;

    public BlocksTypeMapping BTM => typeMapping;
    BlocksTypeMapping typeMapping;

    public GlobalLightSource GLS => globalLightSource;
    GlobalLightSource globalLightSource;

    public SelfWorld World => selfWorld;
    private SelfWorld selfWorld;

    List<Core.IRunnable> allIRunnables;
    public T GetRunnable<T>() where T : class, Core.IRunnable
    {
        foreach(var tmp in allIRunnables)
        {
            if (tmp is T res)
                return res;
        }
        return null;
    }
    #endregion

    #region dispose
    private void OnApplicationQuit()
    {
        foreach(var requestDispose in allIRunnables)
        {
            requestDispose.Dispose();
        }

        //nativeChunks.Clear();
        builtInChunks.Clear();

        System.GC.Collect();
    }
    #endregion

    #region access
    public static Vector3Long GetChunkPos(Vector3Long globalPos)
    {
        var chunk = globalPos / RuntimeChunk.chunkSize;
        if (globalPos.x < 0)
            chunk.x -= 1;
        if (globalPos.y < 0)
            chunk.y -= 1;
        if (globalPos.z < 0)
            chunk.z -= 1;
        return chunk;
    }
    public void SetPlayerPosition(Vector3Long position)
    {
        var ch = GetChunkPos(position);
        selfWorld.PlayerChunkPosition = ch;
    }
    public void FillBlock(Vector3Long chunk, RuntimeBlock? data)
    {
        selfWorld.FillBlock(chunk, data);
    }
    public void FillBlocks(Vector3Long chunk, params RuntimeBlock[] data)
    {
        selfWorld.FillBlocks(chunk, data);
    }
    public RuntimeBlock? ReadBlock(Vector3Long globalIndex)
    {
        return selfWorld.ReadBlock(globalIndex);
    }
    public RuntimeBlock ReadBlockUnsafe(Vector3Long globalIndex)
    {
        return selfWorld.ReadBlockUnsafe(globalIndex);
    }
    #endregion

    #region world
    private uint chunksUpdated = 0;
    const int chunkSize = RuntimeChunk.chunkSize;
    [SerializeField, Header("World Options")] UnityChunk prefChunk;
    //Dictionary<Vector3Long, RuntimeChunk> nativeChunks = new Dictionary<Vector3Long, RuntimeChunk>();
    Dictionary<Vector3Long, UnityChunk> builtInChunks = new Dictionary<Vector3Long, UnityChunk>();
    Vector3Long globalShift = new Vector3Long(0, 0, 0);
    public Vector3 GlobalShift => new Vector3(globalShift.x * chunkSize, globalShift.y * chunkSize, globalShift.z * chunkSize);

    Stopwatch genereWatch;

    #region obsolete
    /*
    IEnumerator FirstGeneration()
    {

        //genereWatch = Stopwatch.StartNew();

        //while(!GeneratePass_0_IsReady)
        //{
        //    printProgress.text = $"Generating [{progress*100:f1}%]";
        //    yield return null;
        //}
        //printProgress.text = "Wait unity sync";
        
        //StartCoroutine(SecondGeneration());
    }

    IEnumerator SecondGeneration()
    {
        int count = 0, updInFrame = 16;
        double thisProgress = 0, der =  1d / nativeChunks.Count;
        foreach (var pair in nativeChunks)
        {
            var _unityChunk = Instantiate(prefChunk);
            _unityChunk.SetRuntimeChunk(pair.Value);
            _unityChunk.Enable();
            thisProgress += der;
            printProgress.text = $"Wait unity sync {thisProgress * 100:f1}%";

            count++;
            if (count >= updInFrame)
            {
                count = 0;
                yield return null;
            }
        }

        yield return null;
        genereWatch.Stop();
        printProgress.text = $"Complete [{genereWatch.Elapsed.TotalSeconds:f2} s]";
        EndGeneration();
    }

    void EndGeneration()
    {
        selfPlayer.position = movePlayerToPoint.position;
    }
    #endregion

    private void GenerateWorld()
    {
        //Generate_SinglePass();
        Generate_DoublePass();
    }

    private double progress;
    #region async
    private bool GeneratePass_0_IsReady = false;
    private async void Generate_SinglePass()
    {
        progress = 0;
        await Task.Run(delegate {
            var progressStep = 1d / (32 * 32 * RuntimeChunk.chunkVolume);

            var grassRef = WorldGenerator.Self.BTM.GetReference("grass");
            var crystalRef = WorldGenerator.Self.BTM.GetReference("crystal ore");
            var stoneRef = WorldGenerator.Self.BTM.GetReference("stone");

            LightModel mdl = new LightModel(100, Vector2.zero, 0.8f);

            for (int chunk_x = 0; chunk_x < 32; chunk_x++)
                for (int chunk_z = 0; chunk_z < 32; chunk_z++)
                {

                    var blocks = new RuntimeBlock[chunkSize, chunkSize, chunkSize];


                    #region mk1
                    //for (int y = 0; y < chunkSize; y++)
                    //    for (int z = 0; z < chunkSize; z++)
                    //        for (int x = 0; x < chunkSize; x++)
                    //        {
                    //            if (y < 16)
                    //            {
                    //                if (x == z)
                    //                {
                    //                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, stoneRef, mdl);
                    //                }
                    //                else
                    //                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                    //            }
                    //            else
                    //                blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);

                    //            if (x % 2 == 0 && y == 20 && z % 2 == 1)
                    //            {
                    //                blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                    //            }
                    //        }
                    #endregion

                    #region mk2
                    for (int y = 0; y < chunkSize; y++)
                        for (int z = 0; z < chunkSize; z++)
                            for (int x = 0; x < chunkSize; x++)
                            {
                                if (y < 5)
                                {
                                    if (x == z)
                                    {
                                        if (x % 2 == 0)
                                            blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, stoneRef, mdl);
                                        else
                                            blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, crystalRef, mdl);
                                    }
                                    else
                                        blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                                }
                                else
                                {
                                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);
                                    //float mult = 3;
                                    //Vector2 globalCoords = new Vector2((chunk_x * chunkSize + x)* mult, (chunk_z * chunkSize + z)* mult);
                                    //var noise = Mathf.PerlinNoise(globalCoords.x, globalCoords.y);

                                    ////var delta = (int)((chunkSize - 5) * noise);

                                    //if (noise<0.6f)
                                    //{
                                    //    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                                    //}
                                    ////else if (noise>=0.6f && noise<0.7f)
                                    ////{
                                    ////    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, crystalRef, mdl);
                                    ////}
                                    //else
                                    //{
                                    //    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);
                                    //}
                                }

                                progress += progressStep;
                            }
                    #endregion

                    var ntCH = new RuntimeChunk(new Vector3Long(chunk_x, 0, chunk_z), blocks);

                    //nativeChunk.FullGenerateMesh();

                    //var _unityChunk = Instantiate(prefChunk);
                    //_unityChunk.SetRuntimeChunk(nativeChunk);
                    //_unityChunk.GetComponent<MeshRenderer>().material = MainMaterial;

                    nativeChunks.Add(new Vector3Long(chunk_x, 0, chunk_z), ntCH);
                }

            GetRunnable<MeshBuilderThread>().Execute();
            GeneratePass_0_IsReady = true;
        });
    }

    async void Generate_DoublePass()
    {
        const int areaSize = 32;

        progress = 0;

        var progressStep = 1d / (areaSize * areaSize * RuntimeChunk.chunkVolume);

        var grassRef = WorldGenerator.Self.BTM.GetReference("grass");
        var crystalRef = WorldGenerator.Self.BTM.GetReference("crystal ore");
        var stoneRef = WorldGenerator.Self.BTM.GetReference("stone");

        LightModel mdl = new LightModel(100, Vector2.zero, 0.8f);

       

        await Task.Run(Pass_0);

        void Pass_0()
        {
            var pass1 = Task.Run(Pass_1);
            var pass2 = Task.Run(Pass_2);
            var pass3 = Task.Run(Pass_3);
            //1
            for (int chunk_x = 0; chunk_x < areaSize/2; chunk_x++)
                for (int chunk_z = 0; chunk_z < areaSize/2; chunk_z++)
                {
                    
                    var blocks = new RuntimeBlock[chunkSize, chunkSize, chunkSize];


                    #region mk1
                    //for (int y = 0; y < chunkSize; y++)
                    //    for (int z = 0; z < chunkSize; z++)
                    //        for (int x = 0; x < chunkSize; x++)
                    //        {
                    //            if (y < 16)
                    //            {
                    //                if (x == z)
                    //                {
                    //                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, stoneRef, mdl);
                    //                }
                    //                else
                    //                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                    //            }
                    //            else
                    //                blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);

                    //            if (x % 2 == 0 && y == 20 && z % 2 == 1)
                    //            {
                    //                blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                    //            }
                    //        }
                    #endregion

                    #region mk2
                    for (int y = 0; y < chunkSize; y++)
                        for (int z = 0; z < chunkSize; z++)
                            for (int x = 0; x < chunkSize; x++)
                            {
                                if (y < 5)
                                {
                                    if (x == z)
                                    {
                                        if (x % 2 == 0)
                                            blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, stoneRef, mdl);
                                        else
                                            blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, crystalRef, mdl);
                                    }
                                    else
                                        blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                                }
                                else
                                {
                                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);
                                    //float mult = 3;
                                    //Vector2 globalCoords = new Vector2((chunk_x * chunkSize + x)* mult, (chunk_z * chunkSize + z)* mult);
                                    //var noise = Mathf.PerlinNoise(globalCoords.x, globalCoords.y);

                                    ////var delta = (int)((chunkSize - 5) * noise);

                                    //if (noise<0.6f)
                                    //{
                                    //    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                                    //}
                                    ////else if (noise>=0.6f && noise<0.7f)
                                    ////{
                                    ////    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, crystalRef, mdl);
                                    ////}
                                    //else
                                    //{
                                    //    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);
                                    //}
                                }

                                progress += progressStep;
                            }
                    #endregion

                    var ntCH = new RuntimeChunk(new Vector3Long(chunk_x, 0, chunk_z), blocks);

                    //nativeChunk.FullGenerateMesh();

                    //var _unityChunk = Instantiate(prefChunk);
                    //_unityChunk.SetRuntimeChunk(nativeChunk);
                    //_unityChunk.GetComponent<MeshRenderer>().material = MainMaterial;

                    nativeChunks.Add(new Vector3Long(chunk_x, 0, chunk_z), ntCH);
                }

            //main task
            pass1.Wait();
            pass2.Wait();
            pass3.Wait();


            GetRunnable<MeshBuilderThread>().Execute();
            GeneratePass_0_IsReady = true;
        }

        void Pass_1()
        {
            //2
            for (int chunk_x = areaSize/2; chunk_x < areaSize; chunk_x++)
                for (int chunk_z = 0; chunk_z < areaSize/2; chunk_z++)
                {

                    var blocks = new RuntimeBlock[chunkSize, chunkSize, chunkSize];


                    #region mk1
                    //for (int y = 0; y < chunkSize; y++)
                    //    for (int z = 0; z < chunkSize; z++)
                    //        for (int x = 0; x < chunkSize; x++)
                    //        {
                    //            if (y < 16)
                    //            {
                    //                if (x == z)
                    //                {
                    //                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, stoneRef, mdl);
                    //                }
                    //                else
                    //                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                    //            }
                    //            else
                    //                blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);

                    //            if (x % 2 == 0 && y == 20 && z % 2 == 1)
                    //            {
                    //                blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                    //            }
                    //        }
                    #endregion

                    #region mk2
                    for (int y = 0; y < chunkSize; y++)
                        for (int z = 0; z < chunkSize; z++)
                            for (int x = 0; x < chunkSize; x++)
                            {
                                if (y < 5)
                                {
                                    if (x == z)
                                    {
                                        if (x % 2 == 0)
                                            blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, stoneRef, mdl);
                                        else
                                            blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, crystalRef, mdl);
                                    }
                                    else
                                        blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                                }
                                else
                                {
                                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);
                                    //float mult = 3;
                                    //Vector2 globalCoords = new Vector2((chunk_x * chunkSize + x)* mult, (chunk_z * chunkSize + z)* mult);
                                    //var noise = Mathf.PerlinNoise(globalCoords.x, globalCoords.y);

                                    ////var delta = (int)((chunkSize - 5) * noise);

                                    //if (noise<0.6f)
                                    //{
                                    //    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                                    //}
                                    ////else if (noise>=0.6f && noise<0.7f)
                                    ////{
                                    ////    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, crystalRef, mdl);
                                    ////}
                                    //else
                                    //{
                                    //    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);
                                    //}
                                }

                                progress += progressStep;
                            }
                    #endregion

                    var ntCH = new RuntimeChunk(new Vector3Long(chunk_x, 0, chunk_z), blocks);

                    //nativeChunk.FullGenerateMesh();

                    //var _unityChunk = Instantiate(prefChunk);
                    //_unityChunk.SetRuntimeChunk(nativeChunk);
                    //_unityChunk.GetComponent<MeshRenderer>().material = MainMaterial;

                    nativeChunks.Add(new Vector3Long(chunk_x, 0, chunk_z), ntCH);
                }

        }

        void Pass_2()
        {
            //3
            for (int chunk_x = 0; chunk_x < areaSize/2; chunk_x++)
                for (int chunk_z = areaSize/2; chunk_z < areaSize; chunk_z++)
                {

                    var blocks = new RuntimeBlock[chunkSize, chunkSize, chunkSize];


                    #region mk1
                    //for (int y = 0; y < chunkSize; y++)
                    //    for (int z = 0; z < chunkSize; z++)
                    //        for (int x = 0; x < chunkSize; x++)
                    //        {
                    //            if (y < 16)
                    //            {
                    //                if (x == z)
                    //                {
                    //                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, stoneRef, mdl);
                    //                }
                    //                else
                    //                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                    //            }
                    //            else
                    //                blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);

                    //            if (x % 2 == 0 && y == 20 && z % 2 == 1)
                    //            {
                    //                blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                    //            }
                    //        }
                    #endregion

                    #region mk2
                    for (int y = 0; y < chunkSize; y++)
                        for (int z = 0; z < chunkSize; z++)
                            for (int x = 0; x < chunkSize; x++)
                            {
                                if (y < 5)
                                {
                                    if (x == z)
                                    {
                                        if (x % 2 == 0)
                                            blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, stoneRef, mdl);
                                        else
                                            blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, crystalRef, mdl);
                                    }
                                    else
                                        blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                                }
                                else
                                {
                                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);
                                    //float mult = 3;
                                    //Vector2 globalCoords = new Vector2((chunk_x * chunkSize + x)* mult, (chunk_z * chunkSize + z)* mult);
                                    //var noise = Mathf.PerlinNoise(globalCoords.x, globalCoords.y);

                                    ////var delta = (int)((chunkSize - 5) * noise);

                                    //if (noise<0.6f)
                                    //{
                                    //    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                                    //}
                                    ////else if (noise>=0.6f && noise<0.7f)
                                    ////{
                                    ////    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, crystalRef, mdl);
                                    ////}
                                    //else
                                    //{
                                    //    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);
                                    //}
                                }

                                progress += progressStep;
                            }
                    #endregion

                    var ntCH = new RuntimeChunk(new Vector3Long(chunk_x, 0, chunk_z), blocks);

                    //nativeChunk.FullGenerateMesh();

                    //var _unityChunk = Instantiate(prefChunk);
                    //_unityChunk.SetRuntimeChunk(nativeChunk);
                    //_unityChunk.GetComponent<MeshRenderer>().material = MainMaterial;

                    nativeChunks.Add(new Vector3Long(chunk_x, 0, chunk_z), ntCH);
                }

        }

        void Pass_3()
        {
            //4
            for (int chunk_x = areaSize / 2; chunk_x < areaSize; chunk_x++)
                for (int chunk_z = areaSize / 2; chunk_z < areaSize; chunk_z++)
                {

                    var blocks = new RuntimeBlock[chunkSize, chunkSize, chunkSize];


                    #region mk1
                    //for (int y = 0; y < chunkSize; y++)
                    //    for (int z = 0; z < chunkSize; z++)
                    //        for (int x = 0; x < chunkSize; x++)
                    //        {
                    //            if (y < 16)
                    //            {
                    //                if (x == z)
                    //                {
                    //                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, stoneRef, mdl);
                    //                }
                    //                else
                    //                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                    //            }
                    //            else
                    //                blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);

                    //            if (x % 2 == 0 && y == 20 && z % 2 == 1)
                    //            {
                    //                blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                    //            }
                    //        }
                    #endregion

                    #region mk2
                    for (int y = 0; y < chunkSize; y++)
                        for (int z = 0; z < chunkSize; z++)
                            for (int x = 0; x < chunkSize; x++)
                            {
                                if (y < 5)
                                {
                                    if (x == z)
                                    {
                                        if (x % 2 == 0)
                                            blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, stoneRef, mdl);
                                        else
                                            blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, crystalRef, mdl);
                                    }
                                    else
                                        blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                                }
                                else
                                {
                                    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);
                                    //float mult = 3;
                                    //Vector2 globalCoords = new Vector2((chunk_x * chunkSize + x)* mult, (chunk_z * chunkSize + z)* mult);
                                    //var noise = Mathf.PerlinNoise(globalCoords.x, globalCoords.y);

                                    ////var delta = (int)((chunkSize - 5) * noise);

                                    //if (noise<0.6f)
                                    //{
                                    //    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, grassRef, mdl);
                                    //}
                                    ////else if (noise>=0.6f && noise<0.7f)
                                    ////{
                                    ////    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, crystalRef, mdl);
                                    ////}
                                    //else
                                    //{
                                    //    blocks[x, y, z] = new RuntimeBlock(new Core.Terrain.Vector3Int(x, y, z), (Rotate)0, 0, mdl);
                                    //}
                                }

                                progress += progressStep;
                            }
                    #endregion

                    var ntCH = new RuntimeChunk(new Vector3Long(chunk_x, 0, chunk_z), blocks);

                    //nativeChunk.FullGenerateMesh();

                    //var _unityChunk = Instantiate(prefChunk);
                    //_unityChunk.SetRuntimeChunk(nativeChunk);
                    //_unityChunk.GetComponent<MeshRenderer>().material = MainMaterial;

                    nativeChunks.Add(new Vector3Long(chunk_x, 0, chunk_z), ntCH);
                }

        }
    }
    */
    #endregion
    //void Gen_Chunk(Vector3Long chunkPos)
    #endregion
}
