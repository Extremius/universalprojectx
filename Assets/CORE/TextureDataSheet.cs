﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Core.Terrain
{
    public class TextureDataSheet
    {
        private Vector2 texcelSize;
        private Texture2D atlas;
        private Vector2 atlasSize;

        Dictionary<uint, BlockUVMapping> mapping;
        

        public TextureDataSheet(Vector2 texcelSize, Texture2D atlas)
        {
            this.texcelSize = texcelSize;
            this.atlas = atlas;
            atlasSize = new Vector2(atlas.width, atlas.height);

            mapping = new Dictionary<uint, BlockUVMapping>();
        }

        const float obs = 1f / 6f;
        public void WriteNewBlockData(string blockName, uint typeRef)
        {
            if (typeRef is 0) return;

            var uvs = new Vector2[6][];
            Vector2Int blockID = new Vector2Int(0, (int)typeRef-1);

            for (int i = 0; i < 6; i++)
            {
                uvs[i] = new Vector2[]
                {
                new Vector2((blockID.x+i) * obs, blockID.y * obs),
                new Vector2((blockID.x+i) * obs, (blockID.y + 1) * obs),
                new Vector2((blockID.x + i + 1) * obs, (blockID.y + 1) * obs),
                new Vector2((blockID.x + i + 1) * obs, blockID.y * obs)
                };
            }

            var newData = new BlockUVMapping(blockName, uvs);
            mapping.Add(typeRef, newData);
        }
        const float derivate = 1f / 256f;
        public void RecalculateLightUVForSide(Vector4[] uv, LightModel.Side mdl)
        {
            var lightTexture = new Vector2(1, 1);
            //var floatStrength = Mathf.Clamp(1f - mdl.intensityOnSide * 0.01f, 0, 1f-derivate);
            byte discretevalue = mdl.intensityOnSide.ToByte; //convert to 256 float presentation
            var floatStrength = 1f - (discretevalue / 256f);
            //var floatStrength = Mathf.Clamp(1f - (discretevalue / 256f), 0, 1f - derivate);

            uv[0] = new Vector4(lightTexture.x * obs, lightTexture.y * obs, floatStrength, 0);
            uv[1] = new Vector4(lightTexture.x * obs, (lightTexture.y + 1) * obs, floatStrength, 1);
            uv[2] = new Vector4((lightTexture.x + 1) * obs, (lightTexture.y + 1) * obs, floatStrength + derivate, 1);
            uv[3] = new Vector4((lightTexture.x + 1) * obs, lightTexture.y * obs, floatStrength + derivate, 0);

        }

        public BlockUVMapping ReadBlockUV(uint typeRef)
        {
            if(mapping.TryGetValue(typeRef, out var result))
            {
                return result;
            }
            return null;
        }
    }

    public class BlocksTypeMapping
    {
        Dictionary<string, uint> nameToTypeReference = new Dictionary<string, uint>();
        Dictionary<uint, string> typeReferenceToName = new Dictionary<uint, string>();
        public uint GetReference(string name)
        {
            if (nameToTypeReference.TryGetValue(name, out var val))
                return val;
            else return 0;
        }
        public string GetName(uint typeReference)
        {
            if (typeReferenceToName.TryGetValue(typeReference, out var val))
                return val;
            else return null;
        }

        public void WriteNewReference(string name, uint typeReference)
        {
            if (typeReference is 0) return;
            if (string.IsNullOrWhiteSpace(name)) return;

            if (!nameToTypeReference.ContainsKey(name))
                if (!typeReferenceToName.ContainsKey(typeReference))
                {
                    nameToTypeReference.Add(name, typeReference);
                    typeReferenceToName.Add(typeReference, name);
                }
        }
    }

    public class BlockUVMapping
    {
        public string Name => blockName;
        string blockName;
        Vector2[] uv_North, uv_East, uv_South, uv_West, uv_Up, uv_Down;
        
        public BlockUVMapping(string name, Vector2[][] uvs)
        {
            blockName = name;
            uv_North = uvs[0];
            uv_East = uvs[1];
            uv_South = uvs[2];
            uv_West = uvs[3];
            uv_Up = uvs[4];
            uv_Down = uvs[5];
        }

        public Vector2[] ReadUV(Rotate side)
        {
            switch (side)
            {
                case Rotate.North:
                    return uv_North;
                case Rotate.East:
                    return uv_East;
                case Rotate.South:
                    return uv_South;
                case Rotate.West:
                    return uv_West;
                case Rotate.Up:
                    return uv_Up;
                case Rotate.Down:
                    return uv_Down;
                default: return null;
            }
        }
    }

    public static class BinaryUtils
    {
        public static byte ConvertFromFloat(float value, float range)
        {
            unchecked { return (byte)(value * 0.256f * range); }
        }

        public static byte ConvertFromFloat_100(float value)
        {
            unchecked { return (byte)(value * 2.56f); }
        }
    }
}
