﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using TMPro;
public class MemoryUsage : MonoBehaviour
{
    const double fromByteToMB = 1d / (1024d * 1024d);
     [SerializeField] TextMeshProUGUI info;
  
    Stopwatch watch = Stopwatch.StartNew();
    private void Update()
    {
        if (watch.ElapsedMilliseconds > 1000)
        {
            watch.Restart();

            var bytesGC = System.GC.GetTotalMemory(true);

            
            info.text = $"Used memory {bytesGC * fromByteToMB:f1} MB";
        }
    }
}
