﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Terrain;
using Core.Light;
using Core;

public class VoxelLightSource : MonoBehaviour
{
    public int bounceCount = 2;
    public long distance;
    public float strength = 100;
    public void CalcLight()
    {
        var world = WorldGenerator.Self.World;

        Vector3Long point = (Vector3Long)transform.position;

        for (long y = -distance; y <= distance; y++)
            for (long z = -distance; z <= distance; z++)
                for (long x = -distance; x <= distance; x++)
                {
                    Vector3Long offset = new Vector3Long(x, y, z);
                    var block = world.ReadBlockUnsafe(point + offset);
                    var mdl = block.Light;

                    mdl.North._localIntensity = (USingleHalf)((strength * (1f / offset.Magnitude)) * 0.01f);
                    mdl.East._localIntensity = (USingleHalf)((strength * (1f / offset.Magnitude)) * 0.01f);
                    mdl.South._localIntensity = (USingleHalf)((strength * (1f / offset.Magnitude)) * 0.01f);
                    mdl.West._localIntensity = (USingleHalf)((strength * (1f / offset.Magnitude)) * 0.01f);
                    mdl.Up._localIntensity = (USingleHalf)((strength * (1f / offset.Magnitude)) * 0.01f);
                    mdl.Down._localIntensity = (USingleHalf)((strength * (1f / offset.Magnitude)) * 0.01f);

                    var chunkPos = WorldGenerator.GetChunkPos(point + offset);
                    if (world.Chunks[chunkPos] is IUnsafeReadWriteDirectlyBlocksData iurwdbd)
                    {
                        iurwdbd.ReadRef(block.localPosition).Light = mdl;
                        iurwdbd.IncrementLightUpd();
                    }
                }


    }
    public void Raycast()
    {
        
       
        List<Vector3Long> chunksPositions = new List<Vector3Long>(4);

        var watch = System.Diagnostics.Stopwatch.StartNew();
        int raysCount = 0;
        for (int x = 0; x < 360; x += 45)
            for (int y = 0; y < 360; y += 45)
            {
                LightRay ray = new LightRay(transform.position, Quaternion.Euler(x,y,0) * Random.rotation, strength, (byte)bounceCount, distance);
                ray.Cast(chunksPositions, 10);
                raysCount++;
            }

        watch.Stop();
        Debug.Log($"{raysCount} Ray casting consume {watch.Elapsed.TotalMilliseconds:f3} ms");
        var world = WorldGenerator.Self.World;
        foreach(var chP in chunksPositions)
        {
            if(world.Chunks[chP] is IUnsafeReadWriteDirectlyBlocksData iurwdbd)
            {
                iurwdbd.IncrementLightUpd();
            }
        }
    }

    private void Start()
    {
        //CalcLight();
        Raycast();
    }
}
