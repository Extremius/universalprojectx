﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controls
{
    enum CameraPresentation
    {
        First_Person,
        Thrid_Person,
    }

    public class CameraControl : MonoBehaviour
    {
        #region consts
        public const string mouseX = "Mouse X";
        public const string mouseY = "Mouse Y";
        #endregion

        #region fields
        [SerializeField] Transform followTo;
        [SerializeField] Vector3 offset;
        [SerializeField] float rotationSensetive = 5;
        [SerializeField] float minYAngle = 5, maxYAngle = 85;
        [SerializeField] CameraPresentation presentation = CameraPresentation.First_Person;
        [SerializeField, Tooltip("0 - отключает сглаживание передвижения")] float smoothMove = 0;
        [SerializeField, Tooltip("0 - отключает сглаживание вращения")] float smoothRotate = 0;
        #endregion
        Vector2 mouseDelta, currentRotation;
        public void SetMouseDelta(Vector2 delta) => mouseDelta = delta;
        public void SetMouseDelta(float x, float y) => mouseDelta = new Vector2(x, y);

        #region props
        Vector2 deltaRotation;
        public Vector2 DeltaRotation => deltaRotation;
        #endregion

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Cursor.lockState = CursorLockMode.Confined;
            }
            switch (presentation)
            {
                case CameraPresentation.First_Person:
                    if(smoothMove is 0)
                    {
                        transform.position = followTo.position + offset;
                    }
                    else
                    {
                        var targetPos = followTo.position + offset;
                        transform.position = Vector3.Lerp(transform.position, targetPos, Time.unscaledDeltaTime * smoothMove);
                    }

                    if(smoothRotate is 0)
                    {

                        var oldRot = currentRotation;

                        currentRotation.x += Input.GetAxis("Mouse X") * rotationSensetive;
                        currentRotation.y -= Input.GetAxis("Mouse Y") * rotationSensetive;
                        currentRotation.x = Mathf.Repeat(currentRotation.x, 360);
                        currentRotation.y = Mathf.Clamp(currentRotation.y, -maxYAngle, maxYAngle);

                        Camera.main.transform.rotation = Quaternion.Euler(currentRotation.y, currentRotation.x, 0);

                        if (Input.GetMouseButtonDown(0))
                            Cursor.lockState = CursorLockMode.Locked;

                        deltaRotation = currentRotation - oldRot;
                    }
                    else
                    {

                    }
                    break;
            }
            
        }
    }
}
