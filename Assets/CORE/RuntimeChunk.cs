﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Core.Binary;
using Core.Light;

namespace Core.Terrain
{
    public interface IUnsafeReadWriteDirectlyBlocksData
    {
        void IncrementMeshUpd();
        ulong MeshUpdates { get; }
        void IncrementLightUpd();
        ulong LightUpdates { get; }

        ref RuntimeBlock ReadRef(Vector3Int position);
        ref RuntimeBlock ReadRef(int x, int y, int z);

        RuntimeBlock Read(Vector3Int position);
        RuntimeBlock Read(int x, int y, int z);
    }
    public class RuntimeChunk : Core.Light.ILightDependet, IMeshBuildSupport, IUnsafeReadWriteDirectlyBlocksData, IDisposable
    {
        //full chunk request minimum 4 620 288 bytes
        public const int chunkSize = 32;
        public const int chunkVolume = chunkSize * chunkSize * chunkSize;

        private ulong meshUpdCount = 0, lightUpdCount =0;

       

        private bool isDirty = true;
        public bool MeshIsDirty => isDirty;
        Vector3Long chunkPosition;
        RuntimeBlock[,,] blocks;
        Dictionary<ushort, Binary.BinaryTreeTag> btt;
        List<Vector3Byte> renderedBlocks = new List<Vector3Byte>(chunkSize*chunkSize);

        #region unsafe
        void IUnsafeReadWriteDirectlyBlocksData.IncrementMeshUpd() => meshUpdCount++;
        void IUnsafeReadWriteDirectlyBlocksData.IncrementLightUpd() => lightUpdCount++;

        ulong IUnsafeReadWriteDirectlyBlocksData.LightUpdates => lightUpdCount;
        ulong IUnsafeReadWriteDirectlyBlocksData.MeshUpdates => meshUpdCount;

        ref RuntimeBlock IUnsafeReadWriteDirectlyBlocksData.ReadRef(Vector3Int position) => ref ReadRef(position);
        ref RuntimeBlock IUnsafeReadWriteDirectlyBlocksData.ReadRef(int x, int y, int z) => ref ReadRef(x,y,z);

        RuntimeBlock IUnsafeReadWriteDirectlyBlocksData.Read(Vector3Int position) => Read(position);
        RuntimeBlock IUnsafeReadWriteDirectlyBlocksData.Read(int x, int y, int z) => Read(x,y,z);
        #endregion

        public static RuntimeChunk LazyCreatedChunk(Vector3Long chunkPosition)
        {
            var ch = new RuntimeChunk();
            ch.chunkPosition = chunkPosition;
            return ch;
        }
        public static void SetBlocksDataIntoLazyChunk(RuntimeChunk ch, RuntimeBlock[,,] data)
        {
            ch.blocks = data;
        }
        private RuntimeChunk()
        {
            WorldGenerator.Self.GetRunnable<LightThread>().Add(this);
            WorldGenerator.Self.GetRunnable<MeshBuilderThread>().Add(this);
            btt = new Dictionary<ushort, BinaryTreeTag>();
        }
        public RuntimeChunk(Vector3Long chunkPosition) : this()
        {
            this.chunkPosition = chunkPosition;
            
            blocks = new RuntimeBlock[chunkSize, chunkSize, chunkSize];

            LightModel lm = new LightModel(100, new Vector2USH(0,0), 0.9f);

            for (byte y = 0; y < chunkSize; y++)
                for (byte z = 0; z < chunkSize; z++)
                    for (byte x = 0; x < chunkSize; x++)
                    {
                        blocks[x, y, z] = new RuntimeBlock(new Vector3Byte(x, y, z), Rotate.North, 0, lm);
                    }
        }
        public RuntimeChunk(Vector3Long chunkPosition, RuntimeBlock[,,] data) : this()
        {
            this.chunkPosition = chunkPosition;

            if (data.GetLength(0) == chunkSize)
                if(data.GetLength(1) == chunkSize)
                    if(data.GetLength(2) == chunkSize)
                    {
                        blocks = data;
                        return;
                    }

            blocks = new RuntimeBlock[chunkSize, chunkSize, chunkSize];
            LightModel lm = new LightModel(100, new Vector2USH(0, 0), 0.9f);

            for (byte y = 0; y < chunkSize; y++)
                for (byte z = 0; z < chunkSize; z++)
                    for (byte x = 0; x < chunkSize; x++)
                    {
                        blocks[x, y, z] = new RuntimeBlock(new Vector3Byte(x, y, z), Rotate.North, 0, lm);
                    }
        }

        #region methods
        public bool MeshUpdated(ref ulong index)
        {
            if (index != meshUpdCount)
            {
                index = meshUpdCount;
                return true;
            }
            else return false;
        }
        public bool LightUpdated(ref ulong index)
        {
            if (index != lightUpdCount)
            {
                index = lightUpdCount;
                return true;
            }
            else return false;
        }
        public Vector3Long ChunkPosition { get => chunkPosition; set => chunkPosition = value; }
        public Vector3Long GlobalBlockPosition(Vector3Int localPosition) => Read(localPosition).Position(chunkPosition);
        private ref RuntimeBlock ReadRef(Vector3Int localPosition) => ref blocks[localPosition.x, localPosition.y, localPosition.z];
        private ref RuntimeBlock ReadRef(int x, int y, int z) => ref blocks[x, y, z];
        public RuntimeBlock Read(Vector3Int localPosition) => blocks[localPosition.x, localPosition.y, localPosition.z];
        public RuntimeBlock Read(int x, int y, int z) => blocks[x, y, z];
        #region btt
        private ushort GetBTTReference(Vector3Int data) => (ushort)((data.x + 1) + data.z * chunkSize + data.y * chunkSize * chunkSize);
        public BinaryTreeTag ReadBTT(Vector3Int blockPos)
        {
            if (disposedValue)
                return null;
            var bttRef = GetBTTReference(blockPos);
            if (btt.TryGetValue(bttRef, out var val))
                return val;
            else return null;
        }
        public BinaryTreeTag ReadBTT(int x, int y, int z)
        {
            if (disposedValue)
                return null;
            var bttRef = GetBTTReference(new Vector3Int(x,y,z));
            if (btt.TryGetValue(bttRef, out var val))
                return val;
            else return null;
        }
        public BinaryTreeTag WriteBTTFor(Vector3Int localPosition)
        {
            if (disposedValue)
                return null;

            var bttRef = GetBTTReference(localPosition);
            if(btt.TryGetValue(bttRef, out var tag))
            {
                return tag;
            }
            else
            { 
                var newbtt = new BinaryTreeTag($"[{chunkPosition.x}; {chunkPosition.y}; {chunkPosition.z}]:{bttRef}");
                btt.Add(bttRef, newbtt);
                //ReadRef(localPosition).bttReference = reference;
                return newbtt;
            }
        }
        #endregion
        public void FillBlocks(params RuntimeBlock[] replacedBlocks)
        {
            if (disposedValue)
                return;

            foreach(var rpBlck in replacedBlocks)
            {
                var pos = rpBlck.localPosition;
                blocks[pos.x, pos.y, pos.z].CopyFrom(rpBlck);

                

                if (pos.x is 0)
                    SetNeighborChunksDirty(Rotate.West);
                else if (pos.x is chunkSize - 1)
                    SetNeighborChunksDirty(Rotate.East);

                if (pos.z is 0)
                    SetNeighborChunksDirty(Rotate.South);
                else if (pos.z is chunkSize - 1)
                    SetNeighborChunksDirty(Rotate.North);

                if (pos.y is 0)
                    SetNeighborChunksDirty(Rotate.Down);
                else if (pos.y is chunkSize - 1)
                    SetNeighborChunksDirty(Rotate.Up);
            }
            if (replacedBlocks.Length > 0)
                isDirty = true;
        }
        public void FillBlock(RuntimeBlock? replacedBlock)
        {
            if (disposedValue)
                return;

            if (replacedBlock.HasValue)
            {
                var blkc = replacedBlock.Value;
                ReadRef(blkc.localPosition).CopyFrom(blkc);

                isDirty = true;

                var pos = blkc.localPosition;

                if (pos.x is 0)
                    SetNeighborChunksDirty(Rotate.West);
                else if (pos.x is chunkSize - 1)
                    SetNeighborChunksDirty(Rotate.East);

                if (pos.z is 0)
                    SetNeighborChunksDirty(Rotate.South);
                else if (pos.z is chunkSize - 1)
                    SetNeighborChunksDirty(Rotate.North);

                if (pos.y is 0)
                    SetNeighborChunksDirty(Rotate.Down);
                else if (pos.y is chunkSize - 1)
                    SetNeighborChunksDirty(Rotate.Up);
            }
        }
        #endregion

        #region mehs

        Vector3[] verties;
        Vector2[] uv0;
        Vector4[] uv2;
        int[] triangles;

        public Vector3[] Verts
        {
            get
            {
                lock(_meshGenerateSync)
                {
                    return verties;
                }
            }
        }
        public Vector2[] UV_0
        {
            get
            {
                lock(_meshGenerateSync)
                {
                    return uv0;
                }
            }
        }
        public Vector4[] UV_2
        {
            get => uv2;
        }
        public int[] Tris
        {
            get
            {
                lock(_meshGenerateSync)
                {
                    return triangles;
                }
            }
        }

        bool IMeshBuildSupport.IsDirty => isDirty;

        private readonly object _meshGenerateSync = new object();

        /// <summary>
        /// Return IReadOnlyList copy
        /// </summary>
        public IReadOnlyList<Vector3Byte> VisibleBlocks()
        {
            if (IsDisposed)
                throw new ObjectDisposedException("Chunk is disposed");

            lock(_meshGenerateSync)
             return renderedBlocks;
        }

        private readonly object _lockForBuildMesh = new object();
        private bool isBuilding = false;
        bool IMeshBuildSupport.IsBuilding => isBuilding;
        public void FullGenerateMesh()
        {
            if (IsDisposed)
                return;

            lock (_lockForBuildMesh)
            {
                isBuilding = true;
                List<Vector3> local_verties = new List<Vector3>(1024);
                List<Vector2> local_uv0 = new List<Vector2>(1024);
                List<Vector4> local_uv2 = new List<Vector4>(1024);
                List<int> local_tris = new List<int>(1024);
                List<Vector3Byte> rndBlocks = new List<Vector3Byte>(1024);

                var TDS = WorldGenerator.Self.TDS;
                var world = WorldGenerator.Self.World;

                Vector2Int lightTexture = new Vector2Int(1, 1);

                for (byte y = 0; y < chunkSize; y++)
                    for (byte z = 0; z < chunkSize; z++)
                        for (byte x = 0; x < chunkSize; x++)
                        {
                            if (IsDisposed)
                                return;

                            var runtimeBlock = Read(x, y, z);
                            if (runtimeBlock.typeReference != 0) //not air or not empty
                            {
                                Vector3 blockPos = new Vector3(x, y, z);
                                var blockPosInt = new Vector3Int(x, y, z);
                                RuntimeBlock? neighborBlock;

                                int numFaces = 0;
                                var blockUV = TDS.ReadBlockUV(runtimeBlock.typeReference);
                                bool requestGen = false;


                                //North
                                if (z < chunkSize - 1)
                                {
                                    if (Read(x, y, z + 1).typeReference is 0)
                                        requestGen = true;
                                }
                                else
                                {
                                    neighborBlock = world.NeighborBlock(GlobalBlockPosition(blockPosInt), Rotate.North);
                                    if (neighborBlock.HasValue ? neighborBlock.Value.typeReference is 0 : false)
                                    {
                                        requestGen = true;
                                        //SetNeighborChunksDirty(Rotate.North);
                                    }
                                }
                                if (requestGen)
                                {
                                    local_verties.Add(blockPos + new Vector3(1, 0, 1));
                                    local_verties.Add(blockPos + new Vector3(1, 1, 1));
                                    local_verties.Add(blockPos + new Vector3(0, 1, 1));
                                    local_verties.Add(blockPos + new Vector3(0, 0, 1));
                                    numFaces++;

                                    local_uv0.AddRange(blockUV.ReadUV(Rotate.North));
                                    //light

                                    TDS.RecalculateLightUVForSide(tmp_uv2, runtimeBlock.Light.North);
                                    local_uv2.AddRange(tmp_uv2);
                                    requestGen = false;
                                }

                                //East
                                if (x < chunkSize - 1)
                                {
                                    if (Read(x + 1, y, z).typeReference is 0)
                                        requestGen = true;
                                }
                                else
                                {
                                    neighborBlock = world.NeighborBlock(GlobalBlockPosition(blockPosInt), Rotate.East);
                                    if (neighborBlock.HasValue ? neighborBlock.Value.typeReference is 0 : false)
                                    {
                                        requestGen = true;
                                        //SetNeighborChunksDirty(Rotate.East);
                                    }
                                }
                                if (requestGen)
                                {
                                    local_verties.Add(blockPos + new Vector3(1, 0, 0));
                                    local_verties.Add(blockPos + new Vector3(1, 1, 0));
                                    local_verties.Add(blockPos + new Vector3(1, 1, 1));
                                    local_verties.Add(blockPos + new Vector3(1, 0, 1));
                                    numFaces++;

                                    local_uv0.AddRange(blockUV.ReadUV(Rotate.East));


                                    TDS.RecalculateLightUVForSide(tmp_uv2, runtimeBlock.Light.East);
                                    local_uv2.AddRange(tmp_uv2);
                                    requestGen = false;
                                }

                                //South
                                if (z > 0)
                                {
                                    if (Read(x, y, z - 1).typeReference is 0)
                                        requestGen = true;
                                }
                                else
                                {
                                    neighborBlock = world.NeighborBlock(GlobalBlockPosition(blockPosInt), Rotate.South);
                                    if (neighborBlock.HasValue ? neighborBlock.Value.typeReference is 0 : false)
                                    {
                                        requestGen = true;
                                        //SetNeighborChunksDirty(Rotate.South);
                                    }
                                }
                                if (requestGen)
                                {

                                    local_verties.Add(blockPos + new Vector3(0, 0, 0));
                                    local_verties.Add(blockPos + new Vector3(0, 1, 0));
                                    local_verties.Add(blockPos + new Vector3(1, 1, 0));
                                    local_verties.Add(blockPos + new Vector3(1, 0, 0));
                                    numFaces++;

                                    local_uv0.AddRange(blockUV.ReadUV(Rotate.South));


                                    TDS.RecalculateLightUVForSide(tmp_uv2, runtimeBlock.Light.South);
                                    local_uv2.AddRange(tmp_uv2);
                                    requestGen = false;
                                }

                                //West
                                if (x > 0)
                                {
                                    if (Read(x - 1, y, z).typeReference is 0)
                                        requestGen = true;
                                }
                                else
                                {
                                    neighborBlock = world.NeighborBlock(GlobalBlockPosition(blockPosInt), Rotate.West);
                                    if (neighborBlock.HasValue ? neighborBlock.Value.typeReference is 0 : false)
                                    {
                                        requestGen = true;
                                        //SetNeighborChunksDirty(Rotate.West);
                                    }
                                }
                                if (requestGen)
                                {
                                    local_verties.Add(blockPos + new Vector3(0, 0, 1));
                                    local_verties.Add(blockPos + new Vector3(0, 1, 1));
                                    local_verties.Add(blockPos + new Vector3(0, 1, 0));
                                    local_verties.Add(blockPos + new Vector3(0, 0, 0));
                                    numFaces++;

                                    local_uv0.AddRange(blockUV.ReadUV(Rotate.West));


                                    TDS.RecalculateLightUVForSide(tmp_uv2, runtimeBlock.Light.West);
                                    local_uv2.AddRange(tmp_uv2);
                                    requestGen = false;
                                }

                                //Up
                                if (y < chunkSize - 1)
                                {
                                    if (Read(x, y + 1, z).typeReference is 0)
                                        requestGen = true;
                                }
                                else
                                {
                                    neighborBlock = world.NeighborBlock(GlobalBlockPosition(blockPosInt), Rotate.Up);
                                    if (neighborBlock.HasValue ? neighborBlock.Value.typeReference is 0 : false)
                                    {
                                        requestGen = true;
                                        //SetNeighborChunksDirty(Rotate.Up);
                                    }
                                }
                                if (requestGen)
                                {
                                    local_verties.Add(blockPos + new Vector3(0, 1, 0));
                                    local_verties.Add(blockPos + new Vector3(0, 1, 1));
                                    local_verties.Add(blockPos + new Vector3(1, 1, 1));
                                    local_verties.Add(blockPos + new Vector3(1, 1, 0));
                                    numFaces++;

                                    local_uv0.AddRange(blockUV.ReadUV(Rotate.Up));


                                    TDS.RecalculateLightUVForSide(tmp_uv2, runtimeBlock.Light.Up);
                                    local_uv2.AddRange(tmp_uv2);
                                    requestGen = false;
                                }

                                //Down
                                if (y > 0)
                                {
                                    if (Read(x, y - 1, z).typeReference is 0)
                                        requestGen = true;
                                }
                                else
                                {
                                    neighborBlock = world.NeighborBlock(GlobalBlockPosition(blockPosInt), Rotate.Down);
                                    if (neighborBlock.HasValue ? neighborBlock.Value.typeReference is 0 : false)
                                    {
                                        requestGen = true;
                                        //SetNeighborChunksDirty(Rotate.Down);
                                    }
                                }
                                if (requestGen)
                                {
                                    local_verties.Add(blockPos + new Vector3(0, 0, 0));
                                    local_verties.Add(blockPos + new Vector3(1, 0, 0));
                                    local_verties.Add(blockPos + new Vector3(1, 0, 1));
                                    local_verties.Add(blockPos + new Vector3(0, 0, 1));
                                    numFaces++;

                                    local_uv0.AddRange(blockUV.ReadUV(Rotate.Down));


                                    TDS.RecalculateLightUVForSide(tmp_uv2, runtimeBlock.Light.Down);
                                    local_uv2.AddRange(tmp_uv2);
                                    requestGen = false;
                                }

                                int tl = local_verties.Count - 4 * numFaces;
                                for (int i = 0; i < numFaces; i++)
                                {
                                    local_tris.AddRange(new int[] { tl + i * 4, tl + i * 4 + 1, tl + i * 4 + 2, tl + i * 4, tl + i * 4 + 2, tl + i * 4 + 3 });
                                }

                                if (numFaces > 0)
                                    rndBlocks.Add(new Vector3Byte(x, y, z));

                            }
                            //else
                            //{
                            //    if (x is 0)
                            //        SetNeighborChunksDirty(Rotate.West);
                            //    else if (x is chunkSize - 1)
                            //        SetNeighborChunksDirty(Rotate.East);

                            //    if (z is 0)
                            //        SetNeighborChunksDirty(Rotate.South);
                            //    else if (z is chunkSize - 1)
                            //        SetNeighborChunksDirty(Rotate.North);

                            //    if (y is 0)
                            //        SetNeighborChunksDirty(Rotate.Down);
                            //    else if (y is chunkSize - 1)
                            //        SetNeighborChunksDirty(Rotate.Up);
                            //}
                        }

                lock (_meshGenerateSync)
                {
                    verties = local_verties.ToArray();
                    uv0 = local_uv0.ToArray();
                    uv2 = local_uv2.ToArray();
                    triangles = local_tris.ToArray();
                    renderedBlocks = rndBlocks;
                }
                isBuilding = false;
                meshUpdCount++;
                lightUpdCount++;
                sendLight = lightUpdCount;
                isDirty = false;
            }
        }

        
        Vector4[] tmp_uv2 = new Vector4[4];//temp buffer for read/write
        public void UpdateLightUV()
        {
            //check validate
            if(uv2.Length != verties.Length)
            {
                Debug.LogError($"Invalid light data, use regenerate mesh fo fix it\nDEBUG: Chunk [{chunkPosition.x}; {chunkPosition.y}; {chunkPosition.z}]");
                return;
            }

            var TDS = WorldGenerator.Self.TDS;

            Vector2Int lightTexture = new Vector2Int(1, 1);
            int offset = 0;

            lock (_meshGenerateSync)
            {
                if (sendLight == lightUpdCount)
                    return;

                //for (int y = 0; y < chunkSize; y++)
                //    for (int z = 0; z < chunkSize; z++)
                //        for (int x = 0; x < chunkSize; x++)
                foreach (var rndPos in renderedBlocks)
                {
                    byte x = rndPos.x, y = rndPos.y, z = rndPos.z;
                    var rtlBlock = Read(rndPos);
                    if (rtlBlock.typeReference != 0)
                    {
                        //North
                        if (z < chunkSize - 1 && Read(x, y, z + 1).typeReference == 0)
                        {
                            //light

                            TDS.RecalculateLightUVForSide(tmp_uv2, rtlBlock.Light.North);
                            WriteRangeIntoArray(in uv2, tmp_uv2, ref offset);
                        }

                        //East
                        if (x < chunkSize - 1 && Read(x + 1, y, z).typeReference == 0) //right is air
                        {
                            TDS.RecalculateLightUVForSide(tmp_uv2, rtlBlock.Light.East);
                            WriteRangeIntoArray(in uv2, tmp_uv2, ref offset);
                        }

                        //South
                        if (z > 0 && Read(x, y, z - 1).typeReference == 0)
                        {
                            TDS.RecalculateLightUVForSide(tmp_uv2, rtlBlock.Light.South);
                            WriteRangeIntoArray(in uv2, tmp_uv2, ref offset);
                        }

                        //West
                        if (x > 0 && Read(x - 1, y, z).typeReference == 0)
                        {
                            TDS.RecalculateLightUVForSide(tmp_uv2, rtlBlock.Light.West);
                            WriteRangeIntoArray(in uv2, tmp_uv2, ref offset);
                        }

                        //Up
                        if (y < chunkSize - 1 && Read(x, y + 1, z).typeReference == 0)
                        {
                            TDS.RecalculateLightUVForSide(tmp_uv2, rtlBlock.Light.Up);
                            WriteRangeIntoArray(in uv2, tmp_uv2, ref offset);
                        }

                        //Down
                        if (y > 0 && Read(x, y - 1, z).typeReference == 0)
                        {
                            TDS.RecalculateLightUVForSide(tmp_uv2, rtlBlock.Light.Down);
                            WriteRangeIntoArray(in uv2, tmp_uv2, ref offset);
                        }
                    }
                }

                lightUpdCount++;
            }

            void WriteRangeIntoArray(in Vector4[] selfArray, Vector4[] otherArray, ref int _offset)
            {
                for (int i = 0; i < otherArray.Length; i++)
                {
                    selfArray[i + _offset] = otherArray[i];
                }
                _offset += otherArray.Length;
            }
        }

        private ulong sendLight = 0;
        bool ILightDependet.RequestUpdate()
        {
            return sendLight != lightUpdCount;
        }
        void ILightDependet.UpdateLight()
        {
            UpdateLightUV();
            sendLight = lightUpdCount;
        }
        public bool IsDisposed => disposedValue;
        void IMeshBuildSupport.BuildMesh()
        {
            FullGenerateMesh();
        }
        void IMeshBuildSupport.SetDirty() => isDirty = true;
        public void SetNeighborChunksDirty(Rotate side)
        {
            var world = WorldGenerator.Self.World;
            RuntimeChunk neighborChunk = null;

            switch (side)
            {
                case Rotate.North:
                    if (world.Chunks.TryGetValue(chunkPosition + new Vector3Long(0, 0, 1), out neighborChunk))
                        (neighborChunk as IMeshBuildSupport).SetDirty();
                    break;
                case Rotate.East:
                    if (world.Chunks.TryGetValue(chunkPosition + new Vector3Long(1, 0, 0), out neighborChunk))
                        (neighborChunk as IMeshBuildSupport).SetDirty();
                    break;
                case Rotate.South:
                    if (world.Chunks.TryGetValue(chunkPosition + new Vector3Long(0, 0, -1), out neighborChunk))
                        (neighborChunk as IMeshBuildSupport).SetDirty();
                    break;
                case Rotate.West:
                    if (world.Chunks.TryGetValue(chunkPosition + new Vector3Long(-1, 0, 0), out neighborChunk))
                        (neighborChunk as IMeshBuildSupport).SetDirty();
                    break;
                case Rotate.Up:
                    if (world.Chunks.TryGetValue(chunkPosition + new Vector3Long(0, 1, 0), out neighborChunk))
                        (neighborChunk as IMeshBuildSupport).SetDirty();
                    break;
                case Rotate.Down:
                    if (world.Chunks.TryGetValue(chunkPosition + new Vector3Long(0, -1, 0), out neighborChunk))
                        (neighborChunk as IMeshBuildSupport).SetDirty();
                    break;
            }
          
        }
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов
        //public bool Disposing => disposedValue;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                lock (_lockForBuildMesh)
                {
                    disposedValue = true;

                    if (disposing)
                    {
                        // TODO: освободить управляемое состояние (управляемые объекты).
                        WorldGenerator.Self.GetRunnable<MeshBuilderThread>().Remove(this);

                    }

                    // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                    // TODO: задать большим полям значение NULL.
                    blocks = null;
                    triangles = null;
                    verties = null;
                    uv0 = null;
                    uv2 = null;
                }
            }
        }

        // TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        // ~RuntimeChunk() {
        //   // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
        //   Dispose(false);
        // }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
